//
//  ApiAccess.h
//  ImageTalk
//
//  Created by Workspace Infotech on 11/2/15.
//  Copyright © 2015 Workspace Infotech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Response.h"
#import "AppDelegate.h"
#import "User.h"

@protocol ApiAccessDelegate <NSObject>

@required
-(void)receivedResponse:(NSDictionary*)data tag:(NSString*) tag index:(int) index;
-(void)receivedError:(JSONModelError*)error tag:(NSString*) tag;
@end


@interface ApiAccess : NSObject
{
    NSUserDefaults *defaults;
    NSString *baseurl;
    BOOL isLogin;
    BOOL isFBLogin;
    User *user;
}


@property (nonatomic,assign) id<ApiAccessDelegate> delegate;
@property (assign,nonatomic) BOOL isContact;

+(ApiAccess*)getSharedInstance;

- (void) initUrls;
- (void) postRequestWithUrl:(NSString*) url params:(NSDictionary*) params tag:(NSString*) tag;
- (void) postRequestWithUrl:(NSString*) url params:(NSDictionary*) params tag:(NSString*) tag index:(int) index;
- (void) getRequestWithUrl:(NSString*) url params:(NSDictionary*) params tag:(NSString*) tag;
- (void) getRequestForGoogleWithParams:(NSDictionary*) params tag:(NSString*) tag;
- (void) setIsLogin:(BOOL) status;
- (void) setIsFBLogin:(BOOL) status;
- (void) setUser:(User*) data;
- (User*) getUser;
- (BOOL) isLogin;
- (BOOL) isFBLogin;
- (void) getRequestWithSocketUrl:(NSString*) url params:(NSDictionary*) params tag:(NSString*) tag;
- (void) postRequestWithSocketUrl:(NSString*) url params:(NSDictionary*) params tag:(NSString*) tag;

@end
