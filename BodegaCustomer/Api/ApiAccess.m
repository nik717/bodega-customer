//
//  ApiAccess.m
//  ImageTalk
//
//  Created by Workspace Infotech on 11/2/15.
//  Copyright © 2015 Workspace Infotech. All rights reserved.
//

#import "ApiAccess.h"
#import "JSONHTTPClient.h"
#import "KVNProgress.h"

static ApiAccess *sharedInstance = nil;

@implementation ApiAccess

+(ApiAccess*)getSharedInstance{
    
    if (!sharedInstance) {
     
       sharedInstance = [[super allocWithZone:NULL]init];
       [sharedInstance initUrls];
    }
    
    return sharedInstance;
}

- (void) initUrls{
    
    defaults = [NSUserDefaults standardUserDefaults];
    baseurl = [defaults objectForKey:@"baseurl"];
    isLogin =  false;
}

- (void) setIsLogin:(BOOL) status
{
    isLogin = status;
}

- (BOOL) isLogin
{
    return  isLogin;
}

- (void) setIsFBLogin:(BOOL) status
{
    isFBLogin = status;
}

- (BOOL) isFBLogin
{
    return  isFBLogin;
}

- (void) postRequestWithUrl:(NSString*) url params:(NSDictionary*) params tag:(NSString*) tag
{
    [self postRequestWithUrl:url params:params tag:tag index:0];
}

- (void) setUser:(User*) data
{
    user = data;
}
- (User*) getUser;
{
    return user;
}

- (void) postRequestWithUrl:(NSString*) url params:(NSDictionary*) params tag:(NSString*) tag index:(int) index
{
    [KVNProgress show];
    
    [JSONHTTPClient postJSONFromURLWithString:[NSString stringWithFormat:@"%@%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"baseurl"],url] params:params
                                   completion:^(NSDictionary *json, JSONModelError *err)
    {
        
        
         if(err)
         {
             [KVNProgress showErrorWithStatus:@"Server Unreachable"];
             [self.delegate receivedError:err tag:tag];
         }
         else
         {
             NSError* error = nil;
             Response *response = [[Response alloc] initWithDictionary:json error:&error];
             
             NSLog(@"%@",json);
             
             if (response.success)
             {
                 [KVNProgress dismiss];
                 [self.delegate receivedResponse:json tag:tag index:index];
                
             }
             else
             {
                
                 
                 if ([tag isEqualToString:@"getSignIn"]) {
                     [KVNProgress showErrorWithStatus:@"The email address or password you entered is incorrect"];
                 }
                 
                 else if ([tag isEqualToString:@"getPass"]) {
                     [KVNProgress showErrorWithStatus:@"We do not have this email address in our system.  Please, verify your correct email or write us to password@mybodega.online"];
                 }
                 else
                 {
                     if (![[ApiAccess getSharedInstance] isFBLogin])
                     {
                         [KVNProgress showErrorWithStatus:@"Wrong Information"];
                     }
                 }

                 
                 
                 
                [self.delegate receivedError:err tag:tag];
             }
             
             
         }
         
         
         
     }];
    
}

- (void) postRequestWithSocketUrl:(NSString*) url params:(NSDictionary*) params tag:(NSString*) tag{
    
    //[KVNProgress show];
    [JSONHTTPClient postJSONFromURLWithString:[NSString stringWithFormat:@"%@%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"chaturl"],url] params:params
                                   completion:^(NSDictionary *json, JSONModelError *err)
     {
         
         
         if(err)
         {
             [KVNProgress showErrorWithStatus:@"Server Unreachable"];
             [self.delegate receivedError:err tag:tag];
         }
         else
         {
            [KVNProgress dismiss];
            [self.delegate receivedResponse:json tag:tag index:0];
            
         }
         
         
         
     }];
    
}

- (void) getRequestWithSocketUrl:(NSString*) url params:(NSDictionary*) params tag:(NSString*) tag
{
    
    //[KVNProgress show];
    [JSONHTTPClient getJSONFromURLWithString:[NSString stringWithFormat:@"%@%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"chaturl"],url] params:params completion:^(id json, JSONModelError *err)
    {
         
         if(err)
         {
            [KVNProgress showErrorWithStatus:@"Server Unreachable"];
            [self.delegate receivedError:err tag:tag];
         }
         else
         {
            [KVNProgress dismiss];
            [self.delegate receivedResponse:json tag:tag index:0];
         }
         
         
     }];
    
}


- (void) getRequestWithUrl:(NSString*) url params:(NSDictionary*) params tag:(NSString*) tag
{
    [JSONHTTPClient getJSONFromURLWithString:[NSString stringWithFormat:@"%@%@",baseurl,url] params:params completion:^(id json, JSONModelError *err)
    {
      
         if(err)
         {
             [self.delegate receivedError:err tag:tag];
         }
         else
         {
             NSError* error = nil;
             Response *response = [[Response alloc] initWithDictionary:json error:&error];
             
             if (response.success) {
                 [self.delegate receivedResponse:json tag:tag index:0];
             }
             else
             {
                [self.delegate receivedError:err tag:tag];
                 
             }
             
             
             
         }

        
     }];
    
}

- (void) getRequestForGoogleWithParams:(NSDictionary*) params tag:(NSString*) tag
{
    [JSONHTTPClient getJSONFromURLWithString:@"https://maps.googleapis.com/maps/api/place/nearbysearch/json" params:params completion:^(id json, JSONModelError *err)
     {
         
         if(err)
         {
             [self.delegate receivedError:err tag:tag];
         }
         else
         {
    
             NSLog(@"%@",json);
            [self.delegate receivedResponse:json tag:tag index:0];
         }
         
         
     }];
    
}








@end
