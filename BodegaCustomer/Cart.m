//
//  Cart.m
//  ShoppingCartExample
//
//  Created by Jose Gustavo Rodriguez Baldera on 5/22/14.
//  Copyright (c) 2014 Jose Gustavo Rodriguez Baldera. All rights reserved.
//

#import "Cart.h"



@interface Cart()
@end

@implementation Cart

+ (double)totalAmount
{
    double total = 0;

    NSMutableArray *contents = [self contents];

    for(CartItem *item in contents)
    {
        total += (item.product.unitprice.intValue * item.quantity);
    }

    return total;
}

- (double) cTotalAmount
{
    double total = 0;
    
    NSMutableArray *copyContent = [self copyContents];
    
    for(CartItem *item in copyContent)
    {
        total += (item.product.unitprice.doubleValue * item.quantity);
    }
    
    return total;
}

+ (int)totalProducts
{
    int total = 0;

    NSMutableArray *contents = [self contents];

    for(CartItem *item in contents)
    {
        total += item.quantity;
    }

    return total;
}

+ (NSMutableArray *)contents
{
    NSMutableArray *content = [[NSMutableArray alloc] init];

    FMDatabase *db = [FMDatabase databaseWithPath:[Db getDatabasePath]];

    [db open];

    FMResultSet *results = [db executeQuery:@"SELECT * FROM cart"];

    while([results next])
    {
        Product *product = [[Product alloc] init];

    
        
        product.id = [results stringForColumn:@"id"];
        product.title = [results stringForColumn:@"title"];
        product.unitprice = [results stringForColumn:@"price"];

        CartItem *item = [[CartItem alloc] initWithProduct:product andQuantity:[results intForColumn:@"quantity"]];

        [content addObject:item];
    }

    [db close];

    return content;
}

- (NSMutableArray *)copyContents
{
    NSMutableArray *copyContent = [[NSMutableArray alloc] init];
    
    FMDatabase *db = [FMDatabase databaseWithPath:[Db getDatabasePath]];
    
    [db open];
    
    FMResultSet *results = [db executeQuery:@"SELECT * FROM cart"];
    
    while([results next])
    {
        Product *product = [[Product alloc] init];
        
        
        
        product.id = [results stringForColumn:@"id"];
        product.title = [results stringForColumn:@"title"];
        product.unitprice = [results stringForColumn:@"price"];
        
        CartItem *item = [[CartItem alloc] initWithProduct:product andQuantity:[results intForColumn:@"quantity"]];
        
        [copyContent addObject:item];
    }
    
    [db close];
    
    return copyContent;

}

+ (CartItem *)getProduct:(int)productid
{
    CartItem *item = nil;

    FMDatabase *db = [FMDatabase databaseWithPath:[Db getDatabasePath]];

    [db open];

    FMResultSet *results = [db executeQuery:[NSString stringWithFormat:@"SELECT * FROM cart where id = %d", productid]];

    while([results next])
    {
        Product *product = [[Product alloc] init];

        product.id = [results stringForColumn:@"id"];
        product.title = [results stringForColumn:@"title"];
        product.unitprice = [results stringForColumn:@"unitprice"];


        item = [[CartItem alloc] initWithProduct:product andQuantity:[results intForColumn:@"quantity"]];
    }

    [db close];

    return item;
}

+ (BOOL)clearProduct:(Product *)product
{
    FMDatabase *db = [FMDatabase databaseWithPath:[Db getDatabasePath]];
    BOOL success = NO;
    
    [db open];
    
    @try
    {
        success =  [db executeUpdate:[NSString stringWithFormat:@"DELETE FROM cart where id = '%@'", product.id]];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Error: %@", [exception reason]);
    }
    @finally
    {
        [db close];
    }
    
    return success;
}

+ (BOOL)removeProduct:(Product *)product
{
    FMDatabase *db = [FMDatabase databaseWithPath:[Db getDatabasePath]];
    BOOL success = NO;

    [db open];

    @try
    {
        success =  [self getQuantity:product] > 1 ? [db executeUpdate:[NSString stringWithFormat:@"UPDATE cart set quantity = %d where id = '%d'", [self getQuantity:product] - 1, product.id.intValue]] : [db executeUpdate:[NSString stringWithFormat:@"DELETE FROM cart where id = '%@'", product.id]];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Error: %@", [exception reason]);
    }
    @finally
    {
        [db close];
    }

    return success;
}

+ (BOOL)exists:(Product *)product
{
    CartItem *item = [self getProduct:product.id.intValue];
    return item != nil;
}

+ (int)getQuantity:(Product *)product
{
    CartItem *item = [self getProduct:product.id.intValue];
    return item != nil ? item.quantity : 0;
}

+ (BOOL)isEmpty
{
    return [self totalProducts] == 0;
}

+ (BOOL)createCart
{
    FMDatabase *db = [FMDatabase databaseWithPath:[Db getDatabasePath]];
    BOOL success = NO;
    
    [db open];
    
    @try
    {
        success = [db executeUpdate:@"CREATE TABLE `cart` ( `id` INTEGER, `title` TEXT, `price` TEXT, `quantity` INTEGER )"];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Error: %@", [exception reason]);
    }
    @finally
    {
        [db close];
    }
    
    return success;
}

+ (BOOL)clearCart
{
    FMDatabase *db = [FMDatabase databaseWithPath:[Db getDatabasePath]];
    BOOL success = NO;

    [db open];

    @try
    {
        success = [db executeUpdate:@"DELETE FROM cart"];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Error: %@", [exception reason]);
    }
    @finally
    {
        [db close];
    }

    return success;
}

+ (BOOL)addProduct:(Product *)product
{
    return [self addProduct:product quantity:1];
}

+ (BOOL)addProduct:(Product *)product quantity:(int)quantity
{
    FMDatabase *db = [FMDatabase databaseWithPath:[Db getDatabasePath]];
    BOOL success = NO;

    success = [db open];

    @try
    {
        CartItem *item = [self getProduct:product.id.intValue];
        success = item ?
                [db executeUpdate:[NSString stringWithFormat:@"UPDATE cart set quantity = %d where id = %d", quantity + item.quantity, product.id.intValue]] :
                [db executeUpdate:[NSString stringWithFormat:@"INSERT INTO cart (id,title,price,quantity) VALUES ('%d','%@','%@','%d')", product.id.intValue,product.title,product.unitprice, quantity]];
        
         NSLog(@"%@", [NSString stringWithFormat:@"INSERT INTO cart (id,title,price,quantity) VALUES ('%d','%@','%@','%d')", product.id.intValue,product.title,product.unitprice, quantity]);
    }
    @catch (NSException *exception)
    {
        NSLog(@"Error: %@", [exception reason]);
    }
    @finally
    {
        [db close];
    }

    return success;
}


@end
