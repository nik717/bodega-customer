//
//  AppDelegate.h
//  BodegaCustomer
//
//  Created by Siam Biswas on 1/31/16.
//  Copyright © 2016 iSoul. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Google/CloudMessaging.h>
#import "User.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate,GGLInstanceIDDelegate, GCMReceiverDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) User *user;
@property (assign, nonatomic) BOOL *isLogin;
@property(nonatomic, readonly, strong) NSString *registrationKey;
@property(nonatomic, readonly, strong) NSString *messageKey;
@property(nonatomic, readonly, strong) NSString *gcmSenderID;
@property(nonatomic, readonly, strong) NSDictionary *registrationOptions;

@property (strong, nonatomic) UITabBarController *tabBarController;
@property (nonatomic,strong) NSString *databasePath;

-(void) createAndCheckDatabase;
-(void) updateCartTabBadge;


@end

