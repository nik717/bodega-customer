//
//  CallViewController.h
//  BodegaCustomer
//
//  Created by Apple on 3/23/16.
//  Copyright © 2016 iSoul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CallViewController : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *callText;
@property (strong, nonatomic) IBOutlet UIImageView *profilePic;
@property (strong, nonatomic) NSString *phoneNumber;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) UIImage *image;

@end
