//
//  CallViewController.m
//  BodegaCustomer
//
//  Created by Apple on 3/23/16.
//  Copyright © 2016 iSoul. All rights reserved.
//

#import "CallViewController.h"
#import "TwilioClient.h"

@interface CallViewController () <TCDeviceDelegate,TCConnectionDelegate>
{
    TCDevice* _phone;
    TCConnection* _connection;
}
@end

@implementation CallViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
     self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    self.profilePic.image = self.image;
    
    self.profilePic.layer.cornerRadius = 60.0;
    [self.profilePic.layer setMasksToBounds:YES];
    
    NSLog(@"Phone Number : %@",self.phoneNumber);
    
    self.phoneNumber = [self.phoneNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    self.phoneNumber = [self.phoneNumber stringByReplacingOccurrencesOfString:@"_" withString:@""];
    
    self.callText.text = [NSString stringWithFormat:@"Connecting with %@...",self.name];
    
   
}

-(void) viewDidAppear:(BOOL)animated
{
    NSString *urlString = [NSString stringWithFormat:@"https://lit-bayou-97655.herokuapp.com/token"];
    NSURL *url = [NSURL URLWithString:urlString];
    NSError *error = nil;
    NSString *token = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:&error];
    
    
    if (token == nil) {
        NSLog(@"Error retrieving token: %@", [error localizedDescription]);
    } else {
        _phone = [[TCDevice alloc] initWithCapabilityToken:token delegate:self];
        self.callText.text = [NSString stringWithFormat:@"Calling %@...",self.name];
        NSDictionary *params = @{@"To": self.phoneNumber};
        _connection = [_phone connect:params delegate:self];
        
    }
}

-(void) connection:(TCConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"sdgasdaga");
}

-(void) connectionDidConnect:(TCConnection *)connection
{
     NSLog(@"faf sdgasdaga");
}

-(void) connectionDidDisconnect:(TCConnection *)connection
{
     self.callText.text = [NSString stringWithFormat:@"Calling failed"];
}

-(void) connectionDidStartConnecting:(TCConnection *)connection
{

}

-(void) viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
    [_connection disconnect];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)hangUp:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)device:(TCDevice *)device didReceivePresenceUpdate:(TCPresenceEvent *)presenceEvent
{
    NSLog(@"sdgasdaga");
}

- (void)device:(TCDevice *)device didReceiveIncomingConnection:(TCConnection *)connection
{
    NSLog(@"Incoming connection from: %@", [connection parameters][@"From"]);
    if (device.state == TCDeviceStateBusy) {
        [connection reject];
    } else {
        [connection accept];
        _connection = connection;
    }
}

- (void)deviceDidStartListeningForIncomingConnections:(TCDevice*)device
{
    NSLog(@"Device: %@ deviceDidStartListeningForIncomingConnections", device);
}

- (void)device:(TCDevice *)device didStopListeningForIncomingConnections:(NSError *)error
{
    
    NSLog(@"Device: %@ didStopListeningForIncomingConnections: %@", device, error);
    self.callText.text = [NSString stringWithFormat:@"Call ended"];
    [self.navigationController popViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
