//
//  CartViewController.h
//  BodegaCustomer
//
//  Created by Siam Biswas on 2/13/17.
//  Copyright © 2017 iSoul. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Cart.h"
#import "CartTableViewCell.h"
#import "AppDelegate.h"

@interface CartViewController : UIViewController <UITableViewDelegate,UITableViewDataSource,CartDelegate>

@property (weak, nonatomic) IBOutlet UIView *specialView;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;
@property (weak, nonatomic) IBOutlet UILabel *emptyLabel;
@property (weak, nonatomic) IBOutlet UIView *totalPriceView;
@property (weak, nonatomic) IBOutlet UILabel *totalPriceLabel;
@property (weak, nonatomic) IBOutlet UITextView *deliveryOptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *cashDeliveryLabel;
@property (weak, nonatomic) IBOutlet UILabel *creditCardLabel;
@property (weak, nonatomic) IBOutlet UILabel *deliveryAmountLabel;
- (IBAction)cashSwitch:(UISwitch *)sender;
- (IBAction)deliverySwitch:(UISwitch *)sender;
- (IBAction)creditSwitch:(UISwitch *)sender;


@end
