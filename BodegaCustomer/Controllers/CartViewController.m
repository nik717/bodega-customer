//
//  CartViewController.m
//  BodegaCustomer
//
//  Created by Siam Biswas on 2/13/17.
//  Copyright © 2017 iSoul. All rights reserved.
//

#import "CartViewController.h"
#import "Cart.h"
#import "CartItem.h"

@interface CartViewController ()

@end

@implementation CartViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
   
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
     [self.tableView reloadData];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - table methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    Cart *cart = [[Cart alloc] init];
    int count = [Cart contents].count;
    double tprice = cart.cTotalAmount;
    NSLog(@"amount value = %f",tprice);
    self.totalPriceLabel.text = [NSString stringWithFormat:@"%.2f", tprice];
    //NSLog(@"string value = %@",);
    [self.tableView setHidden:(count == 0) ? true : false];
    [self.specialView setHidden:(count == 0) ? true : false];
    [self.headerView setHidden:(count == 0) ? true : false];
    [self.nextButton setHidden:(count == 0) ? true : false];
    [self.totalPriceView setHidden:(count == 0) ? true : false];
    
    return count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    CartTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CartCell" forIndexPath:indexPath];
    
    if (cell == nil)
    {
        cell=[[CartTableViewCell alloc] initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:@"CartCell"];
    }
    
    CartItem *item = [Cart contents][indexPath.row];
    cell.product = item.product;
    cell.delegate = self;
    cell.titleLabel.text = item.product.title;
    cell.priceLabel.text = item.product.unitprice;
    cell.quantityLabel.text = [NSString stringWithFormat:@"%d",item.quantity];
    
    
    
    return cell;
    
    
}


-(void) cartUpdate{
    [self.tableView reloadData];
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] updateCartTabBadge];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)cashSwitch:(UISwitch *)sender {
}

- (IBAction)deliverySwitch:(UISwitch *)sender {
}

- (IBAction)creditSwitch:(UISwitch *)sender {
}
@end
