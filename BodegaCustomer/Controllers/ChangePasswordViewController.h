//
//  ChangePasswordViewController.h
//  BodegaCustomer
//
//  Created by Siam Biswas on 2/2/16.
//  Copyright © 2016 iSoul. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ApiAccess.h"

@interface ChangePasswordViewController : UIViewController <ApiAccessDelegate>

@property (strong, nonatomic) IBOutlet UITextField *oldPass;
@property (strong, nonatomic) IBOutlet UITextField *pass;
@property (strong, nonatomic) IBOutlet UITextField *rePass;

@end
