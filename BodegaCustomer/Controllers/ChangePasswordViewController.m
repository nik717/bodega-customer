//
//  ChangePasswordViewController.m
//  BodegaCustomer
//
//  Created by Siam Biswas on 2/2/16.
//  Copyright © 2016 iSoul. All rights reserved.
//

#import "ChangePasswordViewController.h"
#import "KVNProgress.h"

@interface ChangePasswordViewController ()

@end

@implementation ChangePasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    [self.view addGestureRecognizer:singleTap];
    
    [[ApiAccess getSharedInstance]setDelegate:self];
}

- (void)handleSingleTap:(UITapGestureRecognizer *)sender
{
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)done:(id)sender
{
    [self.view endEditing:YES];
    
    if ([self.oldPass.text isEqualToString:@""])
    {
        [KVNProgress showErrorWithStatus:@"Enter Old Password"];
    }
    else if([self.pass.text isEqualToString:@""])
    {
        [KVNProgress showErrorWithStatus:@"Ender New Password"];
        
    }
    else if([self.rePass.text isEqualToString:@""])
    {
        [KVNProgress showErrorWithStatus:@"Re Type New Password"];
        
    }
    else if(![self.pass.text isEqualToString:self.rePass.text])
    {
        [KVNProgress showErrorWithStatus:@"Password dont match"];
        
    }
    else
    {
        NSDictionary *inventory = @{@"userID" : [NSString stringWithFormat:@"%d",[[ApiAccess getSharedInstance] getUser].userId],
                                    @"oldPassword" : self.oldPass.text,
                                    @"newPassword" : self.pass.text,
                                    };
        
        [[ApiAccess getSharedInstance] postRequestWithUrl:@"v2_changePassword.php" params:inventory tag:@"getChange"];
    }

    
}

#pragma mark - ApiAccessDelegate

-(void) receivedResponse:(NSDictionary *)data tag:(NSString *)tag index:(int)index
{
    NSLog(@"%@",tag);
    
    if ([tag isEqualToString:@"getChange"])
    {
        self.oldPass.text = @"";
        self.pass.text = @"";
        self.rePass.text = @"";
        [KVNProgress showSuccessWithStatus:@"Password Change successful"];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    
}

-(void) receivedError:(JSONModelError *)error tag:(NSString *)tag
{
    
    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
