//
//  EmailViewController.h
//  BodegaCustomer
//
//  Created by Siam Biswas on 2/3/16.
//  Copyright © 2016 iSoul. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ApiAccess.h"

@interface EmailViewController : UIViewController <UITableViewDelegate,UITableViewDataSource,ApiAccessDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tableData;
@property (strong, nonatomic) NSMutableArray *myObject;
@property (weak, nonatomic) IBOutlet UIView *empty;

@end
