//
//  EmailViewController.m
//  BodegaCustomer
//
//  Created by Siam Biswas on 2/3/16.
//  Copyright © 2016 iSoul. All rights reserved.
//

#import "EmailViewController.h"
#import "Conversation.h"
#import "TextViewController.h"
#import "SocketAccess.h"

@interface EmailViewController ()

@end

@implementation EmailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    self.tableData.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.empty.hidden = true;
}

-(void)viewDidAppear:(BOOL)animated
{
    [[ApiAccess getSharedInstance] setDelegate:self];
     NSLog(@"%d",[ApiAccess getSharedInstance].getUser.userId);
    [[ApiAccess getSharedInstance] getRequestWithSocketUrl:[NSString stringWithFormat:@"%d?page=1",[ApiAccess getSharedInstance].getUser.userId] params:nil tag:@"recent"];
    [[SocketAccess getSharedInstance] emptyBadge];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

#pragma mark - table methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.myObject.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    if (cell == nil)
    {
        cell=[[UITableViewCell alloc] initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:@"Cell"];
    }
    
    NSError *error;
    ConversationUser *user = [[ConversationUser alloc] initWithDictionary:self.myObject[indexPath.row] error:&error];
    cell.textLabel.text =  (user.sender_id == [ApiAccess getSharedInstance].getUser.userId) ? user.receiver_name : user.sender_name;
    
    
    
    return cell;
    
    
}

#pragma mark - ApiAccessDelegate

-(void) receivedResponse:(NSDictionary *)data tag:(NSString *)tag index:(int)index
{
    
    NSLog(@"TAG %@",tag);
    
    if ([tag isEqualToString:@"recent"])
    {
        
        NSArray *result = [data objectForKey:@"records"];
        self.myObject = [[NSMutableArray alloc]init];
        self.myObject = [result mutableCopy];
        self.empty.hidden = (self.myObject.count>0)?true:false;
        
        [self.tableData reloadData];
    }
    
    
}

-(void) receivedError:(JSONModelError *)error tag:(NSString *)tag
{
    
     self.empty.hidden = (self.myObject.count>0)?true:false;
    
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"details"]) {
        NSIndexPath *indexPath = [self.tableData indexPathForSelectedRow];
        TextViewController *text= segue.destinationViewController;
        NSError *error;
        ConversationUser *user = [[ConversationUser alloc] initWithDictionary:self.myObject[indexPath.row] error:&error];
        text.userId = (user.sender_id == [ApiAccess getSharedInstance].getUser.userId) ? [NSString stringWithFormat:@"%d",user.receiver_id] : [NSString stringWithFormat:@"%d",user.sender_id];
        text.title =  (user.sender_id == [ApiAccess getSharedInstance].getUser.userId) ? user.receiver_name : user.sender_name;
        //text.storePhone = user.user_contact;
        //text.calString = [NSString stringWithFormat:@"Call %@",user.username];
    }
}


@end
