//
//  ForgetPasswordViewController.h
//  BodegaCustomer
//
//  Created by Workspace Infotech on 2/5/16.
//  Copyright © 2016 iSoul. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ApiAccess.h"

@interface ForgetPasswordViewController : UIViewController <ApiAccessDelegate>

@property (strong, nonatomic) IBOutlet UITextField *email;

@end
