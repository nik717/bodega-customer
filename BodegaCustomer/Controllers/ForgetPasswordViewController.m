//
//  ForgetPasswordViewController.m
//  BodegaCustomer
//
//  Created by Workspace Infotech on 2/5/16.
//  Copyright © 2016 iSoul. All rights reserved.
//

#import "ForgetPasswordViewController.h"
#import "JSONHTTPClient.h"
#import "KVNProgress.h"

@interface ForgetPasswordViewController ()

@end

@implementation ForgetPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    [self.view addGestureRecognizer:singleTap];
    
   /* [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShowOrHide:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShowOrHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];*/
    
    
    
    [[ApiAccess getSharedInstance]setDelegate:self];
}

- (void)handleSingleTap:(UITapGestureRecognizer *)sender
{
    [self.view endEditing:YES];
}

-(void)keyboardDidShowOrHide:(NSNotification *)notification
{
    NSDictionary *userInfo = [notification userInfo];
    NSTimeInterval animationDuration;
    UIViewAnimationCurve animationCurve;
    CGRect keyboardEndFrame;
    
    [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
    [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardEndFrame];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:animationDuration];
    [UIView setAnimationCurve:animationCurve];
    
    CGRect newFrame = self.view.frame;
    newFrame.origin.y = keyboardEndFrame.origin.y - newFrame.size.height;
    self.view.frame = newFrame;
    
    [UIView commitAnimations];
}


- (IBAction)submit:(id)sender {
    
    [self.view endEditing:YES];
    
    if ([self.email.text isEqualToString:@""])
    {
        [KVNProgress showErrorWithStatus:@"Email Required"];
    }
    else
    {
        NSDictionary *inventory = @{@"userEmail" : self.email.text,
                                    @"userType" : @"0",
                                    };
        
        [[ApiAccess getSharedInstance] postRequestWithUrl:@"v2_forget-password-request.php" params:inventory tag:@"getPass"];
    }

    
}

- (IBAction)dismiss:(id)sender {
    
    [self.view endEditing:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)contact:(id)sender {
    
      NSString *recipients = @"mailto:info@mybodega.online?subject=Making contact with My Bodega Online LLC";
    NSString *body = @"&body= ";
    
    NSString *email = [NSString stringWithFormat:@"%@%@", recipients, body];
    email = [email stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:email]];
}

- (IBAction)web:(id)sender {
    
     [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"http://www.mybodega.online"]];
}

#pragma mark - ApiAccessDelegate

-(void) receivedResponse:(NSDictionary *)data tag:(NSString *)tag index:(int)index
{
    NSLog(@"%@",tag);
  
    if ([tag isEqualToString:@"getPass"])
    {
        [KVNProgress showSuccessWithStatus:@"Request Submited. Check your email."];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    
}

-(void) receivedError:(JSONModelError *)error tag:(NSString *)tag
{
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
