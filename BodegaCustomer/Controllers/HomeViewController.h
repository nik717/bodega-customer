//
//  HomeViewController.h
//  BodegaCustomer
//
//  Created by Siam Biswas on 2/2/16.
//  Copyright © 2016 iSoul. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Google-Maps-iOS-Utils/GMUMarkerClustering.h>
#import "ApiAccess.h"
#import "AppDelegate.h"

@import GoogleMaps;




@interface HomeViewController : UIViewController <CLLocationManagerDelegate,GMSMapViewDelegate,ApiAccessDelegate,UIAlertViewDelegate,GMUClusterManagerDelegate>
{
    NSUserDefaults *defaults;
    NSString *baseurl;
    CLLocation *currentLocation;
    GMUClusterManager *clusterManager;
}



@property (strong, nonatomic) IBOutlet NSLayoutConstraint *bottomPadding;
@property (weak, nonatomic) IBOutlet UIButton *locBtn;
@property (strong, nonatomic) IBOutlet GMSMapView *mapView;
@property (nonatomic,strong) CLLocationManager *locationManager;
@property (strong, nonatomic) NSString *storeId;
@property (strong, nonatomic) NSMutableDictionary *storeTap;

@end
