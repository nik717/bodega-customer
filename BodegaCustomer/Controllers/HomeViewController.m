//
//  HomeViewController.m
//  BodegaCustomer
//
//  Created by Siam Biswas on 2/2/16.
//  Copyright © 2016 iSoul. All rights reserved.
//

#import "HomeViewController.h"
#import "JSONHTTPClient.h"
#import "StoreResponse.h"
#import "ToastView.h"
#import "StoreViewController.h"
#import "KVNProgress.h"
#import "SocketAccess.h"
#import "POIItem.h"



@interface HomeViewController ()

@end

@implementation HomeViewController

@synthesize locationManager;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [self.locationManager requestWhenInUseAuthorization];
    [self.locationManager startUpdatingLocation];
    self.mapView.delegate = self;
    self.mapView.myLocationEnabled = NO;
    
    self.storeTap = [[NSMutableDictionary alloc] init];
    
    self.locBtn.hidden = true;
    defaults = [NSUserDefaults standardUserDefaults];
    baseurl = [defaults objectForKey:@"baseurl"];
    [SocketAccess getSharedInstance].item = [self.tabBarController.tabBar.items objectAtIndex:2];
    [[ApiAccess getSharedInstance] setDelegate:self];
    
    
    User *user = [self loadCustomObjectWithKey:@"user"];
    NSLog(@"%@",user);
    if (user) {
        
        [[ApiAccess getSharedInstance] setUser:user];
        [[ApiAccess getSharedInstance] setIsLogin:YES];
        [[SocketAccess getSharedInstance] registerOnline];
        
    }
    
   
  //  UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Allow \"My bodega\" to access your location while you use the app?" message:@"We will use this information to show stores near you." delegate:self cancelButtonTitle:@"Don't Allow" otherButtonTitles:@"Allow", nil];
   // [alertView show];
    
    
    
    // Generate and add random items to the cluster manager.
    
    
    // Call cluster() after items have been added
    // to perform the clustering and rendering on map.
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.tabBarController = self.tabBarController;
    [appDelegate updateCartTabBadge];
}

- (void)generateClusterItems:(NSArray <Store>*) shopList {
    
    id<GMUClusterAlgorithm> algorithm =
    [[GMUNonHierarchicalDistanceBasedAlgorithm alloc] init];
    id<GMUClusterIconGenerator> iconGenerator =
    [[GMUDefaultClusterIconGenerator alloc] init];
    id<GMUClusterRenderer> renderer =
    [[GMUDefaultClusterRenderer alloc] initWithMapView:_mapView
                                  clusterIconGenerator:iconGenerator];
    clusterManager =
    [[GMUClusterManager alloc] initWithMap:_mapView
                                 algorithm:algorithm
                                  renderer:renderer];
    
    [clusterManager setDelegate:self mapDelegate:self];

    for (int index = 0; index < shopList.count; index++) {
        Store *store = shopList[index];
        CLLocationCoordinate2D position = CLLocationCoordinate2DMake(store.storeLatitude, store.storeLongitude);
        NSString *name = [NSString stringWithFormat:@"%@", store.storeName];
        id<GMUClusterItem> item =
        [[POIItem alloc] initWithPosition:position
                                     name:name snippet:store.storeStreet icon:[UIImage imageNamed:@"ic_marker"] identity:[NSString stringWithFormat:@"%d",store.storeId]];
        [self.storeTap setValue:@"0" forKey:[NSString stringWithFormat:@"%d",store.storeId]];
        [clusterManager addItem:item];
        
    }
}


-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (buttonIndex == 1)
    {
        self.locBtn.hidden = false;
        self.mapView.myLocationEnabled = YES;
        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:currentLocation.coordinate.latitude
                                                                longitude:currentLocation.coordinate.longitude
                                                                     zoom:16];
        [self.mapView animateToCameraPosition:camera];
    }
}

-(void) viewWillAppear:(BOOL)animated
{
    
  

    
    if (![[ApiAccess getSharedInstance] isLogin])
    {
        self.tabBarController.tabBar.hidden=YES;
        self.bottomPadding.constant = 20.0;
    }
    else
    {
        self.tabBarController.tabBar.hidden=NO;
        self.bottomPadding.constant = 60.0;
    }
}

- (User *)loadCustomObjectWithKey:(NSString *)key {

    NSData *encodedObject = [defaults objectForKey:key];
    User *object = [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
    return object;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)back:(id)sender {
    [self.tabBarController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)myLocation:(id)sender {
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:currentLocation.coordinate.latitude
                                                            longitude:currentLocation.coordinate.longitude
                                                                 zoom:16];
    [self.mapView animateToCameraPosition:camera];
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateToLocation: %@", newLocation);
    currentLocation = newLocation;
    NSLog(@"%f %f",newLocation.coordinate.longitude,newLocation.coordinate.latitude);
    
    self.locBtn.hidden = false;
    self.mapView.myLocationEnabled = YES;
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:currentLocation.coordinate.latitude
                                                            longitude:currentLocation.coordinate.longitude
                                                                 zoom:16];
    [self.mapView animateToCameraPosition:camera];
    
    
    [defaults setValue:[NSString stringWithFormat:@"%f",newLocation.coordinate.longitude] forKey:@"longitude"];
    [defaults setValue:[NSString stringWithFormat:@"%f",newLocation.coordinate.latitude]  forKey:@"latitude"];
    
    
    
   
    NSDictionary *inventory = @{@"myLatitude" : [NSString stringWithFormat:@"%f",newLocation.coordinate.latitude],
                                @"myLongitude" : [NSString stringWithFormat:@"%f",newLocation.coordinate.longitude]
                                };
    
    [[ApiAccess getSharedInstance] postRequestWithUrl:@"v2_nearest-shops.php" params:inventory tag:@"getData"];
    
   
    
    [locationManager stopUpdatingLocation];
}

-(UIView *)mapView:(GMSMapView *)mapView markerInfoWindow:(GMSMarker *)marker {
    UIView *infoWindow  = [[UIView alloc]init];
    infoWindow.frame = CGRectMake(0, 0,220,70);
    infoWindow.backgroundColor = [UIColor whiteColor];
    infoWindow.layer.cornerRadius = 10.0;
    [infoWindow.layer setMasksToBounds:YES];
    
    POIItem *poiItem = marker.userData;
    if (poiItem != nil) {
        int yPosition = 16;
        UILabel *titleLabel =[[UILabel alloc]init];
        titleLabel.frame =CGRectMake(0,yPosition,infoWindow.frame.size.width,20);
        [infoWindow addSubview:titleLabel];
        titleLabel.numberOfLines = 0;
        titleLabel.textAlignment = NSTextAlignmentCenter;
        titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        titleLabel.text = poiItem.name;
        titleLabel.textColor = [UIColor blackColor];
        titleLabel.font = [UIFont systemFontOfSize:16 weight:2.0];
        
        UILabel *snippetLabel = [[UILabel alloc]init];
        snippetLabel.frame =CGRectMake(0,yPosition+20,infoWindow.frame.size.width,20);
        [infoWindow addSubview:snippetLabel];
        snippetLabel.text = poiItem.snippet;
        snippetLabel.font = [UIFont fontWithName:@"Arial" size:14.0];
        snippetLabel.textColor = [UIColor blackColor];
        snippetLabel.textAlignment = NSTextAlignmentCenter;
    }
    
   
    
    return infoWindow;
    
}

-(void) mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate
{
    NSArray *keys = [self.storeTap allKeys];
    
    for (int i = 0; i <[keys count]; i++)
    {
        [self.storeTap setValue:@"0" forKey:[keys objectAtIndex: i]];
    }
}

#pragma mark GMUClusterManagerDelegate



- (void)clusterManager:(GMUClusterManager *)clusterManager didTapCluster:(id<GMUCluster>)cluster {
    GMSCameraPosition *newCamera =
    [GMSCameraPosition cameraWithTarget:cluster.position zoom:_mapView.camera.zoom + 1];
    GMSCameraUpdate *update = [GMSCameraUpdate setCamera:newCamera];
    [_mapView moveCamera:update];
}



- (BOOL) mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker
{
    NSLog(@"Tapped");
    
    [mapView setSelectedMarker:marker];
    
    POIItem *poiItem = marker.userData;
    if (poiItem != nil) {
        NSString *isTap = [self.storeTap objectForKey:poiItem.identity];
        
        if ([isTap isEqualToString:@"1"])
        {
            self.storeId = poiItem.identity;
            [self performSegueWithIdentifier:@"store" sender:self];
        }
        
        [self.storeTap setValue:@"1" forKey:poiItem.identity];
    }

    
    return true;
}

- (void)mapView:(GMSMapView *)mapView didTapInfoWindowOfMarker:(GMSMarker *)marker {
    
    NSLog(@"Tapped");
    
    POIItem *poiItem = marker.userData;
    if (poiItem != nil) {
    
            self.storeId = poiItem.identity;
            [self performSegueWithIdentifier:@"store" sender:self];
    }
}

#pragma mark - ApiAccessDelegate

-(void) receivedResponse:(NSDictionary *)data tag:(NSString *)tag index:(int)index
{
    NSLog(@"%@",tag);
    NSError* error = nil;
    
    if ([tag isEqualToString:@"getData"])
    {
        
        StoreResponse *response = [[StoreResponse alloc] initWithDictionary:data error:&error];
        
        
        if(response.success)
        {
            [self generateClusterItems: response.results.shopList];
            [clusterManager cluster];
        }

       
        
    }
    
}

-(void) receivedError:(JSONModelError *)error tag:(NSString *)tag
{
    


}



#pragma mark - Navigation

 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 
        if ([segue.identifier isEqualToString:@"store"])
        {
            StoreViewController *data = [segue destinationViewController];
            data.storeId = [self.storeId integerValue];
         }
 
 }

@end
