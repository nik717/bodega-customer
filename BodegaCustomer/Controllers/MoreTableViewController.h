//
//  MoreTableViewController.h
//  BodegaCustomer
//
//  Created by Siam Biswas on 2/13/17.
//  Copyright © 2017 iSoul. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface MoreTableViewController : UITableViewController

@end
