//
//  MoreTableViewController.m
//  BodegaCustomer
//
//  Created by Siam Biswas on 2/13/17.
//  Copyright © 2017 iSoul. All rights reserved.
//

#import "MoreTableViewController.h"
#import "KVNProgress.h"
#import "UITabBarController+HideTabBar.h"
#import "ApiAccess.h"
#import "SocketAccess.h"

@interface MoreTableViewController ()

@end

@implementation MoreTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)actionLogout:(id)sender {
    
    if ([[ApiAccess getSharedInstance] isFBLogin])
    {
        FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
        [loginManager logOut];
        
        [FBSDKAccessToken setCurrentAccessToken:nil];
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:@"user"];
    
    [[SocketAccess getSharedInstance] registerOffline];
    [[ApiAccess getSharedInstance]setIsLogin:NO];
    [ApiAccess getSharedInstance].isContact = false;
    [self.tabBarController setSelectedIndex:0];
    self.tabBarController.selectedViewController = [self.tabBarController.viewControllers objectAtIndex:0];
}



@end
