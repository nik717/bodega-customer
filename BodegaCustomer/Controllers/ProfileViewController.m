//
//  ProfileViewController.m
//  BodegaCustomer
//
//  Created by Siam Biswas on 2/2/16.
//  Copyright © 2016 iSoul. All rights reserved.
//

#import "ProfileViewController.h"
#import "KVNProgress.h"

@interface ProfileViewController ()

@end

@implementation ProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    [self.view addGestureRecognizer:singleTap];
    
    
    self.firstName.text = [[ApiAccess getSharedInstance] getUser].userFirstName;
    self.lastName.text = [[ApiAccess getSharedInstance] getUser].userLastName;
    self.email.text = [[ApiAccess getSharedInstance] getUser].userEmail;
    self.contactNo.text = [[ApiAccess getSharedInstance] getUser].userContact;
    
    self.passreView.hidden = [[ApiAccess getSharedInstance] isFBLogin];
    self.passView.hidden = [[ApiAccess getSharedInstance] isFBLogin];
    
     [[ApiAccess getSharedInstance]setDelegate:self];
}

- (void)viewDidAppear:(BOOL)animated {
    
    if ([self.contactNo.text isEqualToString:@""] && ([ApiAccess getSharedInstance].isContact == false))
    {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Update Contact"
                                                        message:@"Please update your contact information for receiving discounts and offers from us"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
        [ApiAccess getSharedInstance].isContact = true;
        
    }
    
}

- (void)handleSingleTap:(UITapGestureRecognizer *)sender
{
    [self.view endEditing:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)edit:(id)sender {
    
    self.done.hidden = !self.done.hidden;
    self.cancel.hidden = !self.cancel.hidden;
    
    [self.firstName setEnabled:!self.firstName.enabled];
    [self.lastName setEnabled:!self.lastName.enabled];
    [self.email setEnabled:!self.email.enabled];
    [self.contactNo setEnabled:!self.contactNo.enabled];
    [self.password setEnabled:!self.password.enabled];
    [self.passwordRe setEnabled:!self.passwordRe.enabled];
    
}

- (IBAction)done:(id)sender {
    
    [self.view endEditing:YES];
    
    if ([self.email.text isEqualToString:@""])
    {
        [KVNProgress showErrorWithStatus:@"Email Required"];
    }
    else if([self.firstName.text isEqualToString:@""])
    {
        [KVNProgress showErrorWithStatus:@"First name Required"];
        
    }
    else if([self.lastName.text isEqualToString:@""])
    {
        [KVNProgress showErrorWithStatus:@"Last name Required"];
        
    }
    else if(![self.password.text isEqualToString:self.passwordRe.text])
    {
        [KVNProgress showErrorWithStatus:@"Password dont match"];
        
    }
    else
    {
        NSDictionary *inventory = @{@"userEmail" : self.email.text,
                                    @"userFirstName" : self.firstName.text,
                                    @"userLastName" : self.lastName.text,
                                    @"userContact" : self.contactNo.text,
                                    @"newPassword" : self.password.text,
                                    @"userType" : @"0",
                                    @"userId" : [NSString stringWithFormat:@"%d",[[ApiAccess getSharedInstance] getUser].userId],
                                   
                                    };
        
        [[ApiAccess getSharedInstance] postRequestWithUrl:@"v2_update_client2.php" params:inventory tag:@"getUpdate"];
    }

    
}

- (IBAction)cancel:(id)sender {
    
    self.done.hidden = !self.done.hidden;
    self.cancel.hidden = !self.cancel.hidden;
    
    [self.firstName setEnabled:!self.firstName.enabled];
    [self.lastName setEnabled:!self.lastName.enabled];
    [self.email setEnabled:!self.email.enabled];
    [self.contactNo setEnabled:!self.contactNo.enabled];
    [self.password setEnabled:!self.password.enabled];
    [self.passwordRe setEnabled:!self.passwordRe.enabled];

}


#pragma mark - ApiAccessDelegate

-(void) receivedResponse:(NSDictionary *)data tag:(NSString *)tag index:(int)index
{
    NSLog(@"%@",tag);
    
    if ([tag isEqualToString:@"getUpdate"])
    {
       
        [KVNProgress showSuccessWithStatus:@"Update successful"];
        
        self.done.hidden = !self.done.hidden;
        self.cancel.hidden = !self.cancel.hidden;
        
        [self.firstName setEnabled:!self.firstName.enabled];
        [self.lastName setEnabled:!self.lastName.enabled];
        [self.email setEnabled:!self.email.enabled];
        [self.contactNo setEnabled:!self.contactNo.enabled];
        [self.password setEnabled:!self.password.enabled];
        [self.passwordRe setEnabled:!self.passwordRe.enabled];

        
        [[ApiAccess getSharedInstance] getUser].userFirstName = self.firstName.text;
        [[ApiAccess getSharedInstance] getUser].userLastName = self.lastName.text;
        [[ApiAccess getSharedInstance] getUser].userEmail = self.email.text;
        [[ApiAccess getSharedInstance] getUser].userContact = self.contactNo.text;
    }
    
}

-(void) receivedError:(JSONModelError *)error tag:(NSString *)tag
{
    
    
    
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
