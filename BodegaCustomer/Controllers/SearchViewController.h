//
//  SearchViewController.h
//  BodegaCustomer
//
//  Created by Siam Biswas on 2/17/17.
//  Copyright © 2017 iSoul. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchTableViewCell.h"
#import "ApiAccess.h"
#import "ProductResponse.h"

@interface SearchViewController : UIViewController <UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,ApiAccessDelegate>

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *myObject;
@property (strong, nonatomic) NSString *storeID;

@end
