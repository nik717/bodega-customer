//
//  SearchViewController.m
//  BodegaCustomer
//
//  Created by Siam Biswas on 2/17/17.
//  Copyright © 2017 iSoul. All rights reserved.
//

#import "SearchViewController.h"

@interface SearchViewController ()

@end

@implementation SearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [[ApiAccess getSharedInstance] setDelegate:self];
    
}

-(void) viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    if (![[ApiAccess getSharedInstance] isLogin])
    {
        self.tabBarController.tabBar.hidden=YES;
    }
    else
    {
        self.tabBarController.tabBar.hidden=NO;
    }
}

-(void) search:(NSString*) key{
    NSDictionary *inventory = @{@"storeid" : self.storeID,
                                @"term" :key
                                };
    
    [[ApiAccess getSharedInstance] postRequestWithUrl:@"productsearch.php" params:inventory tag:@"getData"];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _myObject.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    SearchTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SearchCell" forIndexPath:indexPath];
    
    if (cell == nil)
    {
        cell=[[SearchTableViewCell alloc] initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:@"CartCell"];
    }
    
    Product *product = _myObject[indexPath.row];
    cell.product = product;
    cell.titleLabel.text = product.title;
    cell.priceLabel.text = product.unitprice;
    
    
    
    return cell;
    
    
}



-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
    [self.view endEditing:YES];
    [searchBar setShowsCancelButton:false];
}

-(void) searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    [searchBar setShowsCancelButton:true];
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [self search:searchBar.text];
    [searchBar resignFirstResponder];
    [self.view endEditing:YES];
    [searchBar setShowsCancelButton:false];
}

#pragma mark - ApiAccessDelegate

-(void) receivedResponse:(NSDictionary *)data tag:(NSString *)tag index:(int)index
{
    NSLog(@"%@",data);
    NSError* error = nil;
    
    if ([tag isEqualToString:@"getData"])
    {
        
        ProductResponse *response = [[ProductResponse alloc] initWithDictionary:data error:&error];
        
        
        if(!response.error && response.records.count > 0)
        {
            [self.myObject removeAllObjects];
            self.myObject = [response.records mutableCopy];
            [self.tableView reloadData];
        }
        
        
        
    }
    
}

-(void) receivedError:(JSONModelError *)error tag:(NSString *)tag
{
    
    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
