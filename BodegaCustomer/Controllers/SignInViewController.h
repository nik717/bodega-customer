//
//  SignInViewController.h
//  BodegaCustomer
//
//  Created by Siam Biswas on 2/2/16.
//  Copyright © 2016 iSoul. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "SignUpViewController.h"

#import "ApiAccess.h"

@interface SignInViewController : UIViewController <UITextFieldDelegate,ApiAccessDelegate,FBSDKLoginButtonDelegate,SecondDelegate>


@property (strong, nonatomic) IBOutlet FBSDKLoginButton *login;
@property (weak, nonatomic) IBOutlet UITextField *email;
@property (weak, nonatomic) IBOutlet UITextField *password;
@property (strong, nonatomic) IBOutlet UILabel *name;

@end
