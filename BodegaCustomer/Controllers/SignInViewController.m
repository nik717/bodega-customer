//
//  SignInViewController.m
//  BodegaCustomer
//
//  Created by Siam Biswas on 2/2/16.
//  Copyright © 2016 iSoul. All rights reserved.
//

#import "SignInViewController.h"
#import "JSONHTTPClient.h"
#import "UserResponse.h"
#import "SocketAccess.h"
#import "KVNProgress.h"

@interface SignInViewController ()

@end

@implementation SignInViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
       
    self.name.text = self.title;
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    [self.view addGestureRecognizer:singleTap];
    
  /*[[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShowOrHide:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShowOrHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];*/
    
     self.login.readPermissions = @[@"public_profile", @"email", @"user_friends"];
     self.login.delegate = self;
    
    self.email.delegate = self;
    self.password.delegate = self;
    
    
}

-(void) viewWillAppear:(BOOL)animated
{
    [[ApiAccess getSharedInstance]setDelegate:self];
}

-(void)keyboardDidShowOrHide:(NSNotification *)notification
{
    NSDictionary *userInfo = [notification userInfo];
    NSTimeInterval animationDuration;
    UIViewAnimationCurve animationCurve;
    CGRect keyboardEndFrame;
    
    [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
    [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardEndFrame];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:animationDuration];
    [UIView setAnimationCurve:animationCurve];
    
    CGRect newFrame = self.view.frame;
    newFrame.origin.y = keyboardEndFrame.origin.y - newFrame.size.height;
    self.view.frame = newFrame;
    
    [UIView commitAnimations];
}


-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        // Not found, so remove keyboard.
        [textField resignFirstResponder];
    }
    return NO; // We do not want UITextField to insert line-breaks.
}



- (void)handleSingleTap:(UITapGestureRecognizer *)sender
{
    [self.view endEditing:YES];
}

- (IBAction)dismiss:(id)sender {
    
     [self.view endEditing:YES];
     [self dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)signUp:(id)sender {
    
    [self performSegueWithIdentifier:@"signUp" sender:nil];

}

- (IBAction)signIn:(id)sender {
    
    [self.view endEditing:YES];
    
    if ([self.email.text isEqualToString:@""])
    {
        [KVNProgress showErrorWithStatus:@"Email Required"];
    }
    else if([self.password.text isEqualToString:@""])
    {
        [KVNProgress showErrorWithStatus:@"Password Required"];

    }
    else
    {
        //NSLog(@"Bodega ");
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        NSLog(@"Bodega ");
        NSDictionary *inventory = @{@"userEmail" : self.email.text,
                                    @"userPassword" : self.password.text,
                                    @"userType" : @"0",
                                    @"userrlMethod" : @"0",
                                    @"newGcmCode": [defaults objectForKey:@"gcmcode"]
                                    };
         //NSLog(@"Details: %@", [inventory description]);
        
         [[ApiAccess getSharedInstance] postRequestWithUrl:@"v2_login.php" params:inventory tag:@"getSignIn"];
        
        //[[ApiAccess getSharedInstance] postRequestWithUrl:@"v2_login.php" params:inventory tag:@"getSignIn"];
        
        // NSLog(@"New details: %@", [inventory description]);
    }
    
    

}


- (void)loginButton:(FBSDKLoginButton *)loginButton
didCompleteWithResult:(FBSDKLoginManagerLoginResult *)result
              error:(NSError *)error {
    
    NSLog(@"Logges in");
    if (error) {
        // Process error
    }
    else if (result.isCancelled) {
        // Handle cancellations
    }
    else {
        
        
        NSLog(@"Logges in");
        NSDictionary *param=@{@"fields": @"id,first_name,last_name,email"};
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:param]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
             
             if (!error) {
                 NSLog(@"fetched user:%@  and Email : %@", result,result[@"email"]);
                 NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                 NSDictionary *inventory = @{@"userEmail" : result[@"email"],
                                             @"userPassword" : @"",
                                             @"userType" : @"0",
                                             @"userrlMethod" : @"1",
                                             @"newGcmCode": [defaults objectForKey:@"gcmcode"] ? [defaults objectForKey:@"gcmcode"] : @"",
                                             @"userSocialId" : result[@"id"],
                                             };
                 
                 [[ApiAccess getSharedInstance] setIsFBLogin:YES];
                 [[ApiAccess getSharedInstance] postRequestWithUrl:@"v2_login.php" params:inventory tag:@"getSignIn"];
                 
             }
         }];
        
    }
}

- (void) loginButtonDidLogOut:(FBSDKLoginButton *)loginButton
{
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - ApiAccessDelegate

-(void) receivedResponse:(NSDictionary *)data tag:(NSString *)tag index:(int)index
{
    
    NSError* error = nil;
    
    if ([tag isEqualToString:@"getSignIn"])
    {
        UserResponse  *response = [[UserResponse alloc] initWithDictionary:data error:&error];
        NSLog(@"%@",response.toJSONString);
        [[ApiAccess getSharedInstance] setUser:response.results.userInformation];
        [[ApiAccess getSharedInstance] setIsLogin:YES];
        [[SocketAccess getSharedInstance] registerOnline];
        
        [self saveCustomObject:response.results.userInformation key:@"user"];
       
        [self dismissViewControllerAnimated:YES completion:nil];
        
    }
    

}

-(void) receivedError:(JSONModelError *)error tag:(NSString *)tag
{
    
    
    
}

- (void)secondViewControllerDismissed:(NSString *)email pass:(NSString*) pass
{
    
    NSLog(@"%@",email);
    
    if ([[ApiAccess getSharedInstance] isFBLogin])
    {
        self.email.text = email;
        self.password.text = pass;
        
        NSDictionary *inventory = @{@"userEmail" : email,
                                    @"userPassword" : @"",
                                    @"userType" : @"0",
                                    @"userrlMethod" : @"1",
                                    @"userSocialId" : pass,
                                    };

        [[ApiAccess getSharedInstance] postRequestWithUrl:@"v2_login.php" params:inventory tag:@"getSignIn"];

    }
    else
    {
        self.email.text = email;
        self.password.text = pass;
        
        NSDictionary *inventory = @{@"userEmail" : email,
                                    @"userPassword" : pass,
                                    @"userType" : @"0",
                                    @"userrlMethod" : @"0",
                                    };
        
        [[ApiAccess getSharedInstance] postRequestWithUrl:@"v2_login.php" params:inventory tag:@"getSignIn"];
    }
   
}

- (void)saveCustomObject:(User *)object key:(NSString *)key {
    NSData *encodedObject = [NSKeyedArchiver archivedDataWithRootObject:object];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:encodedObject forKey:key];
    [defaults synchronize];
    
}




#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"signUp"])
    {
        SignUpViewController *data = [segue destinationViewController];
        data.myDelegate = self;
      
        
    }
    
}


@end
