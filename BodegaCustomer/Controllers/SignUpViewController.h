//
//  SignUpViewController.h
//  BodegaCustomer
//
//  Created by Siam Biswas on 2/2/16.
//  Copyright © 2016 iSoul. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ApiAccess.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>


@protocol SecondDelegate <NSObject>
-(void) secondViewControllerDismissed:(NSString *) email pass:(NSString*) pass;

@end



@interface SignUpViewController : UIViewController <UITextFieldDelegate,ApiAccessDelegate>


@property (nonatomic, assign) id<SecondDelegate>  myDelegate;


@property (weak, nonatomic) IBOutlet UIButton *privacy;
@property (strong, nonatomic) IBOutlet UITextField *email;
@property (strong, nonatomic) IBOutlet UITextField *password;
@property (strong, nonatomic) IBOutlet UITextField *firstName;
@property (strong, nonatomic) IBOutlet UITextField *lastName;
@property (strong, nonatomic) IBOutlet UITextField *contactNo;

@end
