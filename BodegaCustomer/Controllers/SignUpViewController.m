//
//  SignUpViewController.m
//  BodegaCustomer
//
//  Created by Siam Biswas on 2/2/16.
//  Copyright © 2016 iSoul. All rights reserved.
//

#import "SignUpViewController.h"
#import "UserResponse.h"
#import "KVNProgress.h"


@interface SignUpViewController ()

@end

@implementation SignUpViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    [self.view addGestureRecognizer:singleTap];
    _privacy.titleLabel.textAlignment = NSTextAlignmentCenter;
    
    /*[[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShowOrHide:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShowOrHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];*/
    
    [[ApiAccess getSharedInstance]setDelegate:self];
    
    self.email.delegate = self;
    self.password.delegate = self;
    self.firstName.delegate = self;
    self.lastName.delegate = self;
    self.contactNo.delegate = self;
}

-(void)keyboardDidShowOrHide:(NSNotification *)notification
{
    NSDictionary *userInfo = [notification userInfo];
    NSTimeInterval animationDuration;
    UIViewAnimationCurve animationCurve;
    CGRect keyboardEndFrame;
    
    [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
    [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardEndFrame];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:animationDuration];
    [UIView setAnimationCurve:animationCurve];
    
    CGRect newFrame = self.view.frame;
    newFrame.origin.y = keyboardEndFrame.origin.y - newFrame.size.height;
    self.view.frame = newFrame;
    
    [UIView commitAnimations];
}

-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        // Not found, so remove keyboard.
        [textField resignFirstResponder];
    }
    return NO; // We do not want UITextField to insert line-breaks.
}

- (void)handleSingleTap:(UITapGestureRecognizer *)sender
{
    [self.view endEditing:YES];
}


- (IBAction)signIn:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];

    
}

- (IBAction)privacyPolicy:(id)sender {
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"http://www.mybodega.online/terms"]];
}


- (IBAction)loginButtonClicked:(id)sender {
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logInWithReadPermissions:@[@"public_profile", @"email", @"user_friends"] handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        if (error) {
            // Process error
            NSLog(@"error %@",error);
        } else if (result.isCancelled) {
            // Handle cancellations
            NSLog(@"Cancelled");
        } else {
            if ([result.grantedPermissions containsObject:@"email"]) {
                // Do work
                [self fetchUserInfo];
            }
        }
    }];
}

-(void)fetchUserInfo {
    
    if ([FBSDKAccessToken currentAccessToken]) {
        
        NSLog(@"Token is available");
        
        NSDictionary *param=@{@"fields": @"id,first_name,last_name,email"};
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:param]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
             if (!error) {
                 NSLog(@"Fetched User Information:%@", result);
                 
                 NSDictionary *inventory = @{@"userEmail" : result[@"email"],
                                             @"userPassword" : @"",
                                             @"userFirstName" : result[@"first_name"],
                                             @"userLastName" : result[@"last_name"],
                                             @"userContact" : @"",
                                             @"userType" : @"0",
                                             @"userrlMethod" : @"1",
                                             @"userDeviceType" : @"1",
                                             @"userSocialId" : result[@"id"],
                                             @"userDeviceId" : [[[UIDevice currentDevice] identifierForVendor] UUIDString],
                                             };
                 
                 [[ApiAccess getSharedInstance] setIsFBLogin:YES];
                 self.email.text = result[@"email"];
                 self.password.text = result[@"id"];
                 [[ApiAccess getSharedInstance] postRequestWithUrl:@"v2_registration.php" params:inventory tag:@"getSignUp"];
                 
                 
             }
             else {
                 NSLog(@"Error %@",error);
             }
         }];
        
    } else {
        
        NSLog(@"User is not Logged in");
    }
}



- (IBAction)signUp:(id)sender {
    
    [self.view endEditing:YES];
    
    if ([self.email.text isEqualToString:@""])
    {
        [KVNProgress showErrorWithStatus:@"Email Required"];
    }
    else if([self.password.text isEqualToString:@""])
    {
        [KVNProgress showErrorWithStatus:@"Password Required"];
        
    }
    else if([self.firstName.text isEqualToString:@""])
    {
        [KVNProgress showErrorWithStatus:@"First name Required"];
        
    }
    else if([self.lastName.text isEqualToString:@""])
    {
        [KVNProgress showErrorWithStatus:@"Last name Required"];
        
    }
    else if([self.contactNo.text isEqualToString:@""])
    {
        [KVNProgress showErrorWithStatus:@"Contact no Required"];
        
    }
    else
    {
        NSDictionary *inventory = @{@"userEmail" : self.email.text,
                                    @"userPassword" : self.password.text,
                                    @"userFirstName" : self.firstName.text,
                                    @"userLastName" : self.lastName.text,
                                    @"userContact" : self.contactNo.text,
                                    @"userType" : @"0",
                                    @"userrlMethod" : @"0",
                                    @"userDeviceType" : @"1",
                                    @"userDeviceId" : [[[UIDevice currentDevice] identifierForVendor] UUIDString],
                                    };
        
        [[ApiAccess getSharedInstance] postRequestWithUrl:@"v2_registration.php" params:inventory tag:@"getSignUp"];
        
        
    }
    
}

- (IBAction)dismiss:(id)sender {
    
    [self.view endEditing:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - ApiAccessDelegate

-(void) receivedResponse:(NSDictionary *)data tag:(NSString *)tag index:(int)index
{
    NSLog(@"%@",tag);
   
    if ([tag isEqualToString:@"getSignUp"]){
    
        
        [KVNProgress showSuccessWithStatus:@"Registration Successful"];
        [self.myDelegate secondViewControllerDismissed:self.email.text pass:self.password.text];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    
}

-(void) receivedError:(JSONModelError *)error tag:(NSString *)tag
{
    if ([[ApiAccess getSharedInstance] isFBLogin])
    {
        [KVNProgress showSuccessWithStatus:@"Registration successful"];
        [self.myDelegate secondViewControllerDismissed:self.email.text pass:self.password.text];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    
    self.email.text = @"";
    self.password.text = @"";
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
