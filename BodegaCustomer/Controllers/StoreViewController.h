//
//  StoreViewController.h
//  BodegaCustomer
//
//  Created by Siam Biswas on 2/2/16.
//  Copyright © 2016 iSoul. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ApiAccess.h"
#import "StoreDetailsResponse.h"
#import "UIImageView+WebCache.h"
#import "SearchViewController.h"


@import GoogleMaps;

@interface StoreViewController : UIViewController <ApiAccessDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (assign, nonatomic) int storeId;
@property (strong, nonatomic) NSString *storePhone;
@property (strong, nonatomic) UILabel *storeName;
@property (strong, nonatomic) UILabel *storeLocation;
@property (strong, nonatomic) IBOutlet UIImageView *storePic;
@property (strong, nonatomic) IBOutlet UILabel *storeManager;
@property (strong, nonatomic) IBOutlet UILabel *optionOne;
@property (strong, nonatomic) IBOutlet UILabel *optionTwo;
@property (strong, nonatomic) IBOutlet UILabel *optionThree;
@property (strong, nonatomic) IBOutlet UILabel *optionFour;
@property (strong, nonatomic) IBOutlet UILabel *mondayHours;
@property (strong, nonatomic) IBOutlet UILabel *tuesdayHours;
@property (strong, nonatomic) IBOutlet UILabel *wednesdayHours;
@property (strong, nonatomic) IBOutlet UILabel *thursdayHours;
@property (strong, nonatomic) IBOutlet UILabel *fridayHours;
@property (strong, nonatomic) IBOutlet UILabel *saturdayHours;
@property (strong, nonatomic) IBOutlet UILabel *sundayHours;
@property (strong, nonatomic) IBOutlet GMSMapView *mpView;
@property (strong, nonatomic) IBOutlet UILabel *call;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightContrain;
@property (weak, nonatomic) IBOutlet UIView *itemView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;


@property (strong, nonatomic) StoreDetailsResponse  *response;

@end
