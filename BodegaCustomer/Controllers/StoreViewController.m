//
//  StoreViewController.m
//  BodegaCustomer
//
//  Created by Siam Biswas on 2/2/16.
//  Copyright © 2016 iSoul. All rights reserved.
//

#import "StoreViewController.h"
#import "KVNProgress.h"
#import "TextViewController.h"
#import "SignInViewController.h"
#import "CallViewController.h"
#import "TimeManagement.h"
#import "ItemCell.h"
#import "Cart.h"


@interface StoreViewController ()

@end

@implementation StoreViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    
    CGRect headerTitleSubtitleFrame = CGRectMake(0, 0, 200, 44);
    UIView* _headerTitleSubtitleView = [[UILabel alloc] initWithFrame:headerTitleSubtitleFrame];
    _headerTitleSubtitleView.backgroundColor = [UIColor clearColor];
    _headerTitleSubtitleView.autoresizesSubviews = NO;
    
    CGRect titleFrame = CGRectMake(0, 2, 200, 24);
    self.storeName = [[UILabel alloc] initWithFrame:titleFrame];
    self.storeName.backgroundColor = [UIColor clearColor];
    self.storeName.font = [UIFont systemFontOfSize:18 weight:2.0];
    self.storeName.textAlignment = NSTextAlignmentCenter;
    self.storeName.textColor = [UIColor whiteColor];
    self.storeName.text =[NSString stringWithFormat:@"Store Name"];
    self.storeName.adjustsFontSizeToFitWidth = YES;
    [_headerTitleSubtitleView addSubview:self.storeName];
    
    CGRect subtitleFrame = CGRectMake(0, 24, 200, 44-24);
    self.storeLocation = [[UILabel alloc] initWithFrame:subtitleFrame];
    self.storeLocation.backgroundColor = [UIColor clearColor];
    self.storeLocation.font = [UIFont boldSystemFontOfSize:11];
    self.storeLocation.textAlignment = NSTextAlignmentCenter;
    self.storeLocation.textColor = [UIColor whiteColor];
    self.storeLocation.text = @"Location";
    self.storeLocation.adjustsFontSizeToFitWidth = YES;
    [_headerTitleSubtitleView addSubview:self.storeLocation];
    
    self.navigationItem.titleView = _headerTitleSubtitleView;
    
    
    
    NSLog(@"%@",[NSString stringWithFormat:@"%d",self.storeId]);
    
    [[ApiAccess getSharedInstance] setDelegate:self];
    
    NSDictionary *inventory = @{@"storeId" : [NSString stringWithFormat:@"%d",self.storeId]};
    [[ApiAccess getSharedInstance] postRequestWithUrl:@"v2_shop-details.php" params:inventory tag:@"getDetailsData"];
    
    self.mpView.settings.scrollGestures = NO;
    
   

}


-(void) viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    if (![[ApiAccess getSharedInstance] isLogin])
    {
        self.tabBarController.tabBar.hidden=YES;
    }
    else
    {
        self.tabBarController.tabBar.hidden=NO;
    }
}


- (IBAction)showText:(id)sender {
    
    if ([[ApiAccess getSharedInstance] isLogin])
    {
        [self performSegueWithIdentifier:@"showText" sender:self];
    }
    else
    {
        [self performSegueWithIdentifier:@"login" sender:self];
    }
}

- (IBAction)call:(id)sender{
    
    if ([[ApiAccess getSharedInstance] isLogin])
    {
        if (self.storePhone)
        {
          [self performSegueWithIdentifier:@"call" sender:self];
        }
        else
        {
            [KVNProgress showErrorWithStatus:@"Contact information not available"];
        }
    }
    else
    {
        [self performSegueWithIdentifier:@"login" sender:self];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - ApiAccessDelegate

-(void) receivedResponse:(NSDictionary *)data tag:(NSString *)tag index:(int)index
{
    NSLog(@"%@",tag);
    NSError* error = nil;
    
    if ([tag isEqualToString:@"getDetailsData"])
    {
        
       self.response = [[StoreDetailsResponse alloc] initWithDictionary:data error:&error];
       self.storeName.text = _response.results.storeDetails.storeName;
       self.storeLocation.text = _response.results.storeDetails.storeStreet;
       self.storePhone = _response.results.storeDetails.storeContact;
        
        
        self.heightContrain.constant = self.response.results.itemlist.count > 0 ? ceil(self.response.results.itemlist.count/2) * 70 + 60 : 50;
        [self updateViewConstraints];
        
        
        
        
        [self.collectionView reloadData];
        
       self.call.text = [NSString stringWithFormat:@"Call %@ %@",_response.results.storeDetails.storeOwnerFirstName, _response.results.storeDetails.storeOwnerLastName];
        
        
       self.storeManager.text = [NSString stringWithFormat:@"Manager : %@ %@",_response.results.storeDetails.storeOwnerFirstName, _response.results.storeDetails.storeOwnerLastName];
        
        
        BOOL open = false;
        
        NSDate *now = [NSDate date];
        NSLocale *twelveHourLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        dateFormatter.locale = twelveHourLocale;
        [dateFormatter setDateFormat:@"EEEE"];
        NSString *day = [dateFormatter stringFromDate:now];
        [dateFormatter setDateFormat:@"a"];
        NSString *currentA = [dateFormatter stringFromDate:now];
        [dateFormatter setDateFormat:@"hh"];
        float current = [[dateFormatter stringFromDate:now] floatValue];
        
        
        
        float start =0 ;
        float end = 0;
        NSString *startA = @"AM";
        NSString *endA = @"AM";
        
        NSLog(@"%@ %@ %f",day,currentA,current);
        
        if ([day isEqualToString:@"Saturday"])
        {
            
            start = [[_response.results.storeDetails.storeHours.SaturDay.startTime.time stringByReplacingOccurrencesOfString:@" " withString:@""] floatValue];
            end = [[_response.results.storeDetails.storeHours.SaturDay.endTime.time stringByReplacingOccurrencesOfString:@" " withString:@""] floatValue];
            startA = [[_response.results.storeDetails.storeHours.SaturDay.startTime.ind stringByReplacingOccurrencesOfString:@" " withString:@""] uppercaseString];
            endA = [[_response.results.storeDetails.storeHours.SaturDay.endTime.ind stringByReplacingOccurrencesOfString:@" " withString:@""] uppercaseString];
            
        }
        
        if ([day isEqualToString:@"Sunday"])
        {
            start = [[_response.results.storeDetails.storeHours.SunDay.startTime.time stringByReplacingOccurrencesOfString:@" " withString:@""] floatValue];
            end = [[_response.results.storeDetails.storeHours.SunDay.endTime.time stringByReplacingOccurrencesOfString:@" " withString:@""] floatValue];
            startA = [[_response.results.storeDetails.storeHours.SunDay.startTime.ind stringByReplacingOccurrencesOfString:@" " withString:@""] uppercaseString];
            endA = [[_response.results.storeDetails.storeHours.SunDay.endTime.ind stringByReplacingOccurrencesOfString:@" " withString:@""] uppercaseString];        }
        
        if ([day isEqualToString:@"Monday"])
        {
            start = [[_response.results.storeDetails.storeHours.MonDay.startTime.time stringByReplacingOccurrencesOfString:@" " withString:@""] floatValue];
            end = [[_response.results.storeDetails.storeHours.MonDay.endTime.time stringByReplacingOccurrencesOfString:@" " withString:@""] floatValue];
            startA = [[_response.results.storeDetails.storeHours.MonDay.startTime.ind stringByReplacingOccurrencesOfString:@" " withString:@""] uppercaseString];
            endA = [[_response.results.storeDetails.storeHours.MonDay.endTime.ind stringByReplacingOccurrencesOfString:@" " withString:@""] uppercaseString];
            
        }
        
        if ([day isEqualToString:@"Tuesday"])
        {
            start = [[_response.results.storeDetails.storeHours.TuesDay.startTime.time stringByReplacingOccurrencesOfString:@" " withString:@""] floatValue];
            end = [[_response.results.storeDetails.storeHours.TuesDay.endTime.time stringByReplacingOccurrencesOfString:@" " withString:@""] floatValue];
            startA = [[_response.results.storeDetails.storeHours.TuesDay.startTime.ind stringByReplacingOccurrencesOfString:@" " withString:@""] uppercaseString];
            endA = [[_response.results.storeDetails.storeHours.TuesDay.endTime.ind stringByReplacingOccurrencesOfString:@" " withString:@""] uppercaseString];
            
        }
        
        if ([day isEqualToString:@"Wednesday"])
        {
            start = [[_response.results.storeDetails.storeHours.WednesDay.startTime.time stringByReplacingOccurrencesOfString:@" " withString:@""] floatValue];
            end = [[_response.results.storeDetails.storeHours.WednesDay.endTime.time stringByReplacingOccurrencesOfString:@" " withString:@""] floatValue];
            startA = [[_response.results.storeDetails.storeHours.WednesDay.startTime.ind stringByReplacingOccurrencesOfString:@" " withString:@""] uppercaseString];
            endA = [[_response.results.storeDetails.storeHours.WednesDay.endTime.ind stringByReplacingOccurrencesOfString:@" " withString:@""] uppercaseString];
        }
        
        if ([day isEqualToString:@"Thursday"])
        {
            start = [[_response.results.storeDetails.storeHours.ThursDay.startTime.time stringByReplacingOccurrencesOfString:@" " withString:@""] floatValue];
            end = [[_response.results.storeDetails.storeHours.ThursDay.endTime.time stringByReplacingOccurrencesOfString:@" " withString:@""] floatValue];
            startA = [[_response.results.storeDetails.storeHours.ThursDay.startTime.ind stringByReplacingOccurrencesOfString:@" " withString:@""] uppercaseString];
            endA = [[_response.results.storeDetails.storeHours.ThursDay.endTime.ind stringByReplacingOccurrencesOfString:@" " withString:@""] uppercaseString];        }
        
        if ([day isEqualToString:@"Friday"])
        {
            start = [[_response.results.storeDetails.storeHours.FriDay.startTime.time stringByReplacingOccurrencesOfString:@" " withString:@""] floatValue];
            end = [[_response.results.storeDetails.storeHours.FriDay.endTime.time stringByReplacingOccurrencesOfString:@" " withString:@""] floatValue];
            startA = [[_response.results.storeDetails.storeHours.FriDay.startTime.ind stringByReplacingOccurrencesOfString:@" " withString:@""] uppercaseString];
            endA = [[_response.results.storeDetails.storeHours.FriDay.endTime.ind stringByReplacingOccurrencesOfString:@" " withString:@""] uppercaseString];
        }
        
        NSLog(@"%@ %@ %f %f",startA,endA,start,end);
        
        
        
        if((current >= start && [currentA isEqualToString:startA]) || (current <= start && ![currentA isEqualToString:startA]))
        {
            if((current >= end && ![currentA isEqualToString:endA]) || current <= end)
            {
                open = true;
            }
        }
        
        
        NSString *startStr = [[NSString stringWithFormat:@"%0.2f %@",start,[startA lowercaseString]] stringByReplacingOccurrencesOfString:@"." withString:@":"];
        NSString *endStr = [[NSString stringWithFormat:@"%0.2f %@",end,[endA lowercaseString]] stringByReplacingOccurrencesOfString:@"." withString:@":"];
        startStr = (start < 10.0) ? [NSString stringWithFormat:@"0%@",startStr] : startStr;
        endStr = (end < 10.0) ? [NSString stringWithFormat:@"0%@",endStr] : endStr;
        
        self.optionOne.text = ([[TimeManagement getSharedInstance] isTimeInMiddle:startStr lastTime:endStr])?[NSString stringWithFormat:@"Store Open"]:[NSString stringWithFormat:@"Store Close"];
        self.optionTwo.text = [NSString stringWithFormat:@"Payment : Cash"];
        
        
        NSLog(@"%@ %@",startStr,endStr);
        
        
        if (_response.results.storeDetails.storeDeliveryFess.dZero.deliveryOption == 1)
        {
            self.optionThree.text = [NSString stringWithFormat:@"No Delivery"];
            self.optionFour.text =@"";
        }
        
        
        if (_response.results.storeDetails.storeDeliveryFess.dOne.deliveryOption == 1)
        {
            self.optionThree.text = [NSString stringWithFormat:@"Free Delivery"];
            self.optionFour.text = (_response.results.storeDetails.storeDeliveryFess.dFour.deliveryOption) ? [NSString stringWithFormat:@"Rush Delivery : $%@",_response.results.storeDetails.storeDeliveryFess.dFour.value]:@"";
        }
        
        if (_response.results.storeDetails.storeDeliveryFess.dTwo.deliveryOption == 1)
        {
            self.optionThree.text = [NSString stringWithFormat:@"Minimum : $%@",_response.results.storeDetails.storeDeliveryFess.dTwo.value];
            self.optionFour.text = (_response.results.storeDetails.storeDeliveryFess.dFour.deliveryOption) ? [NSString stringWithFormat:@"Rush Delivery : $%@",_response.results.storeDetails.storeDeliveryFess.dFour.value]:@"";
        }
        
        if (_response.results.storeDetails.storeDeliveryFess.dThree.deliveryOption == 1)
        {
            self.optionThree.text = [NSString stringWithFormat:@"Regular Charge : $%@",_response.results.storeDetails.storeDeliveryFess.dThree.value];
            self.optionFour.text = (_response.results.storeDetails.storeDeliveryFess.dFour.deliveryOption) ? [NSString stringWithFormat:@"Rush Delivery : $%@",_response.results.storeDetails.storeDeliveryFess.dFour.value]:@"";
        }
        
        if (_response.results.storeDetails.storeDeliveryFess.dFour.deliveryOption ==1 && _response.results.storeDetails.storeDeliveryFess.dOne.deliveryOption == 0&& _response.results.storeDetails.storeDeliveryFess.dTwo.deliveryOption == 0 && _response.results.storeDetails.storeDeliveryFess.dThree.deliveryOption == 0)
        {
            self.optionThree.text = [NSString stringWithFormat:@"Rush Delivery : $%@",_response.results.storeDetails.storeDeliveryFess.dFour.value];
            self.optionFour.text = @"";
        }
       
        
        
       self.mondayHours.text = [NSString stringWithFormat:@"%@ %@ - %@ %@",_response.results.storeDetails.storeHours.MonDay.startTime.time,_response.results.storeDetails.storeHours.MonDay.startTime.ind,_response.results.storeDetails.storeHours.MonDay.endTime.time,_response.results.storeDetails.storeHours.MonDay.endTime.ind];
       self.tuesdayHours.text = [NSString stringWithFormat:@"%@ %@ - %@ %@",_response.results.storeDetails.storeHours.TuesDay.startTime.time,_response.results.storeDetails.storeHours.TuesDay.startTime.ind,_response.results.storeDetails.storeHours.TuesDay.endTime.time,_response.results.storeDetails.storeHours.TuesDay.endTime.ind];
       self.wednesdayHours.text = [NSString stringWithFormat:@"%@ %@ - %@ %@",_response.results.storeDetails.storeHours.WednesDay.startTime.time,_response.results.storeDetails.storeHours.WednesDay.startTime.ind,_response.results.storeDetails.storeHours.WednesDay.endTime.time,_response.results.storeDetails.storeHours.WednesDay.endTime.ind];
       self.thursdayHours.text = [NSString stringWithFormat:@"%@ %@ - %@ %@",_response.results.storeDetails.storeHours.ThursDay.startTime.time,_response.results.storeDetails.storeHours.ThursDay.startTime.ind,_response.results.storeDetails.storeHours.ThursDay.endTime.time,_response.results.storeDetails.storeHours.ThursDay.endTime.ind];
       self.fridayHours.text = [NSString stringWithFormat:@"%@ %@ - %@ %@",_response.results.storeDetails.storeHours.FriDay.startTime.time,_response.results.storeDetails.storeHours.FriDay.startTime.ind,_response.results.storeDetails.storeHours.FriDay.endTime.time,_response.results.storeDetails.storeHours.FriDay.endTime.ind];
       self.saturdayHours.text = [NSString stringWithFormat:@"%@ %@ - %@ %@",_response.results.storeDetails.storeHours.SaturDay.startTime.time,_response.results.storeDetails.storeHours.SaturDay.startTime.ind,_response.results.storeDetails.storeHours.SaturDay.endTime.time,_response.results.storeDetails.storeHours.SaturDay.endTime.ind];
       self.sundayHours.text = [NSString stringWithFormat:@"%@ %@ - %@ %@",_response.results.storeDetails.storeHours.SunDay.startTime.time,_response.results.storeDetails.storeHours.SunDay.startTime.ind,_response.results.storeDetails.storeHours.SunDay.endTime.time,_response.results.storeDetails.storeHours.SunDay.endTime.ind];
        
       
        [[SDImageCache sharedImageCache] removeImageForKey:_response.results.storeImages.profilePic fromDisk:YES];
        __block UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        activityIndicator.center = CGPointMake(CGRectGetMidX(self.storePic.bounds), CGRectGetMidY(self.storePic.bounds));
        activityIndicator.hidesWhenStopped = YES;
        [self.storePic addSubview:activityIndicator];
        [activityIndicator startAnimating];
        [self.storePic sd_setImageWithURL:[NSURL URLWithString:_response.results.storeImages.profilePic]
                         placeholderImage:nil
                                completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                    self.storePic.layer.cornerRadius = 40.0;
                                    [self.storePic.layer setMasksToBounds:YES];
                                    [activityIndicator stopAnimating];
                                }];
        
        //[self.storePic sd_setImageWithURL:[NSURL URLWithString:_response.results.storeImages.profilePic] placeholderImage:nil];
        

        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:_response.results.storeDetails.storeLatitude
                                                                longitude:_response.results.storeDetails.storeLongitude
                                                                     zoom:16];
        self.mpView.camera = camera;
        
        CLLocationCoordinate2D position = CLLocationCoordinate2DMake(_response.results.storeDetails.storeLatitude,_response.results.storeDetails.storeLongitude);
        GMSMarker *marker = [GMSMarker markerWithPosition:position];
        marker.title = _response.results.storeDetails.storeName;
        marker.icon = [UIImage imageNamed:@"ic_marker"];
        marker.map = self.mpView;
        
        
        
        
        
       
    }
    
}

-(void) receivedError:(JSONModelError *)error tag:(NSString *)tag
{
    
    
    
}



#pragma mark - UICollectionView

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
   
    
   
    
    return self.response.results.itemlist.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"Cell";
    
    ItemCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    Product *item = self.response.results.itemlist[indexPath.row];
    
    cell.title.text = item.title;
    cell.subtitle.text = [NSString stringWithFormat:@"price: %@",item.unitprice];
    
    return cell;
}

- (void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if ([ApiAccess getSharedInstance].isLogin){
        Product *item = self.response.results.itemlist[indexPath.row];
        [Cart addProduct:item];
        
        ItemCell *cell = (ItemCell*)[collectionView cellForItemAtIndexPath:indexPath];
        
        [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             cell.view.backgroundColor = [cell.view.backgroundColor colorWithAlphaComponent:0.2];
                             
                         }
                         completion:nil];
        [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             cell.view.backgroundColor = [cell.view.backgroundColor colorWithAlphaComponent:1.0];
                             
                         }
                         completion:nil];
        
        
        [(AppDelegate *)[[UIApplication sharedApplication] delegate] updateCartTabBadge];
    }
   
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
     return CGSizeMake(self.collectionView.frame.size.width/2-5, 70.0);
}


#pragma mark - Navigation
 
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 
    if ([segue.identifier isEqualToString:@"showText"])
    {
        TextViewController *data = [segue destinationViewController];
        data.title = self.storeName.text;
        data.userId = [NSString stringWithFormat:@"%d",self.response.results.storeDetails.storeId];
        data.storePic = self.storePic.image;
        data.storePhone = self.response.results.storeDetails.storeContact;
        data.calString = [NSString stringWithFormat:@"Call %@ %@",_response.results.storeDetails.storeOwnerFirstName, _response.results.storeDetails.storeOwnerLastName];;
     }
     
     if ([segue.identifier isEqualToString:@"call"])
     {
         CallViewController *data = [segue destinationViewController];
         data.title = [NSString stringWithFormat:@"Calling %@",self.storeName.text];
         data.image = self.storePic.image;
         
         data.phoneNumber = ([self.response.results.storeDetails.storeActiveContact isEqualToString:@"userContact"]) ? self.response.results.storeDetails.storeContact : self.response.results.storeDetails.storeLandLine;
         data.name = [NSString stringWithFormat:@"%@ %@",_response.results.storeDetails.storeOwnerFirstName, _response.results.storeDetails.storeOwnerLastName];;
     }
     
     if ([segue.identifier isEqualToString:@"login"])
     {
         SignInViewController *data = [segue destinationViewController];
         data.title = [NSString stringWithFormat:@"%@ %@",_response.results.storeDetails.storeOwnerFirstName, _response.results.storeDetails.storeOwnerLastName];;
       
     }
     
     if ([segue.identifier isEqualToString:@"search"])
     {
         SearchViewController *data = [segue destinationViewController];
         data.storeID = [NSString stringWithFormat:@"%d",_response.results.storeDetails.storeId];
         
     }
 
 }

@end
