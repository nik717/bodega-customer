//
//  TextViewController.h
//  BodegaCustomer
//
//  Created by Siam Biswas on 2/3/16.
//  Copyright © 2016 iSoul. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ApiAccess.h"
#import "SocketAccess.h"


@interface TextViewController : UIViewController <UIActionSheetDelegate,UITableViewDelegate,UITableViewDataSource,ApiAccessDelegate,SocketAccessDelegate>

@property (strong, nonatomic) IBOutlet UIButton *storePicView;
@property (strong, nonatomic) IBOutlet UITableView *tableData;
@property (strong, nonatomic) IBOutlet UITextField *type;
@property (strong, nonatomic) NSMutableArray *myObject;

@property (strong, nonatomic) NSString *userId;
@property (strong, nonatomic) NSString *conversation;
@property (strong, nonatomic) UIImage *storePic;
@property (strong, nonatomic) NSString *calString;
@property (strong, nonatomic) NSString *storePhone;



@end
