//
//  TextViewController.m
//  BodegaCustomer
//
//  Created by Siam Biswas on 2/3/16.
//  Copyright © 2016 iSoul. All rights reserved.
//

#import "TextViewController.h"
#import "KVNProgress.h"
#import "SenderTableViewCell.h"
#import "SenderSingleTableViewCell.h"
#import "ReceiverTableViewCell.h"
#import "ReceiverSingleTableViewCell.h"
#import "ConversationUser.h"
#import "CallViewController.h"

@interface TextViewController ()

@end

@implementation TextViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    [self.view addGestureRecognizer:singleTap];
    
     self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    [self.storePicView setImage:self.storePic forState:UIControlStateNormal];
    self.storePicView.imageView.contentMode =UIViewContentModeScaleAspectFill;
    self.storePicView.layer.cornerRadius = 20.0;
    [self.storePicView.layer setMasksToBounds:YES];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShowOrHide:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShowOrHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    self.myObject = [[NSMutableArray alloc] init];
   
    
    
    [[ApiAccess getSharedInstance] setDelegate:self];
    [[SocketAccess getSharedInstance] setDelegate:self];
    [[SocketAccess getSharedInstance] connect];
    
    [[ApiAccess getSharedInstance] getRequestWithSocketUrl:[NSString stringWithFormat:@"%d/%@?page=1",[ApiAccess getSharedInstance].getUser.userId,self.userId] params:nil tag:@"conversation"];
    


  
}

-(void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[SocketAccess getSharedInstance] setDelegate:nil];
}

- (void)handleSingleTap:(UITapGestureRecognizer *)sender
{
    [self.view endEditing:YES];
}

-(void)keyboardDidShowOrHide:(NSNotification *)notification
{
    NSDictionary *userInfo = [notification userInfo];
    NSTimeInterval animationDuration;
    UIViewAnimationCurve animationCurve;
    CGRect keyboardEndFrame;
    
    [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
    [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardEndFrame];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:animationDuration];
    [UIView setAnimationCurve:animationCurve];
    
    CGRect newFrame = self.view.frame;
    newFrame.origin.y = keyboardEndFrame.origin.y - newFrame.size.height;
    self.view.frame = newFrame;
    
    [UIView commitAnimations];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)call:(id)sender {
    
    [self.view endEditing:YES];
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:self.calString
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Call", nil];
    
    [actionSheet showInView:self.view];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{

    if (buttonIndex == 0) {
        
        if (self.storePhone)
        {
            NSString *URLString = [@"tel://" stringByAppendingString:self.storePhone];
            NSURL *URL = [NSURL URLWithString:URLString];
            [[UIApplication sharedApplication] openURL:URL];
        }
        else
        {
            [KVNProgress showErrorWithStatus:@"Contact information not available"];
        }
        
    }
   
}

- (IBAction)send:(id)sender {
    
    [self.view endEditing:YES];
    
    ConversationUser *data = [[ConversationUser alloc]init];
    
    data.sender_id = [[ApiAccess getSharedInstance] getUser].userId;
    data.message =  self.type.text;
    data.date = [NSString stringWithFormat:@"%@",[self AgoStringFromTime:[NSDate new]]];
    [self.myObject addObject:data];
    
    
    [self.tableData reloadData];
    
    NSIndexPath* ipath = [NSIndexPath indexPathForRow:self.myObject.count-1 inSection:0];
    [self.tableData scrollToRowAtIndexPath: ipath atScrollPosition: UITableViewScrollPositionBottom animated: YES];
    

    
    
    NSLog(@"%@",[NSString stringWithFormat:@"%d",[[ApiAccess getSharedInstance] getUser].userId]);
    
    
    NSDictionary *inventory = @{@"sender" :[NSString stringWithFormat:@"%d",[[ApiAccess getSharedInstance] getUser].userId],
                                @"receiver" :self.userId,
                                @"message" : self.type.text,
                                };
    
    [[SocketAccess getSharedInstance] sendMsg:inventory];
    
    
    self.type.text = @"";

    
}

-(NSString*) AgoStringFromTime : (NSDate*) dateTime
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    
    return  [dateFormatter stringFromDate:dateTime];
}

-(NSString*) toLocalTime: (NSString*) date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    //[dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    
    NSDate *toDate = [dateFormatter dateFromString:date];
    
    NSTimeZone *tz = [NSTimeZone defaultTimeZone];
    NSInteger seconds = [tz secondsFromGMTForDate: toDate];
    NSDate *newDate = [NSDate dateWithTimeInterval: seconds sinceDate: toDate];
    
    //[dateFormatter setTimeZone:[NSTimeZone defaultTimeZone]];
    
    return [dateFormatter stringFromDate:newDate];
}


#pragma mark - table methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.myObject.count;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSError *error;
    ConversationUser *data;
    
    if([self.myObject[indexPath.row] isKindOfClass:[ConversationUser class]])
    {
        data = self.myObject[indexPath.row];
    }
    else
    {
        data = [[ConversationUser alloc] initWithDictionary:self.myObject[indexPath.row] error:&error];
    }
    
    CGSize stringSize = [data.message sizeWithAttributes:@{NSFontAttributeName: [UIFont systemFontOfSize:17.0f]}];
    
    if (data.sender_id != [self.userId intValue]) {
        
        if (stringSize.width + 100 > self.view.frame.size.width) {
            
            SenderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Sender" forIndexPath:indexPath];
            
            if (cell == nil) {
                cell=[[SenderTableViewCell alloc] initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:@"Sender"];
            }
            
            cell.text.text = data.message;
            cell.time.text = [self toLocalTime:data.date];
            
            return cell;
            
        }
        else
        {
            SenderSingleTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SenderSingle" forIndexPath:indexPath];
            
            if (cell == nil) {
                cell=[[SenderSingleTableViewCell alloc] initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:@"Sender"];
            }
            
            cell.text.text = data.message;
            cell.time.text = [self toLocalTime:data.date];


            return cell;
        }
        
        
    }
    else
    {
        
        if (stringSize.width + 100 > self.view.frame.size.width) {
            
            ReceiverTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Receiver" forIndexPath:indexPath];
            
            if (cell == nil) {
                cell=[[ReceiverTableViewCell alloc] initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:@"Receiver"];
            }
            
            cell.text.text = data.message;
            cell.time.text = [self toLocalTime:data.date];

            
            
            
            return cell;
        }
        else
        {
            ReceiverSingleTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ReceiverSingle" forIndexPath:indexPath];
            
            if (cell == nil) {
                cell=[[ReceiverSingleTableViewCell alloc] initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:@"ReceiverSingle"];
            }
            
            cell.text.text = data.message;
            cell.time.text = [self toLocalTime:data.date];

            
            
            return cell;
            
        }
    }

    
    
    
}

#pragma mark - ApiAccessDelegate

-(void) receivedResponse:(NSDictionary *)data tag:(NSString *)tag index:(int)index
{

    NSLog(@"TAG %@",tag);
    
    if ([tag isEqualToString:@"conversation"])
    {
        NSArray *messages = [data objectForKey:@"records"];
        self.storePhone = [data objectForKey:@"sender_contact"];
        self.myObject = [[[messages reverseObjectEnumerator] allObjects] mutableCopy];
        [self.tableData reloadData];
        
        if (self.myObject.count > 0) {
            
             [self.tableData scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:self.myObject.count-1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
            
        }
       
    }
    
    
}

-(void) receivedError:(JSONModelError *)error tag:(NSString *)tag
{
    
    
    [KVNProgress dismiss];
}

#pragma mark - SocketAccessDelegate
-(void)receivedResponseWith:(NSDictionary*)data tag:(NSString*) tag;
{
    NSLog(@"%@",data);
    
    if ([tag isEqualToString:@"chat.messages"])
    {
        [[ApiAccess getSharedInstance] getRequestWithSocketUrl:[NSString stringWithFormat:@"%d/%@?page=1",[ApiAccess getSharedInstance].getUser.userId,self.userId] params:nil tag:@"conversation"];
        
    }
}





#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"call"])
    {
        CallViewController *data = [segue destinationViewController];
        data.title = self.title;
        data.image = self.storePic;
        data.phoneNumber = self.storePhone;
        data.name = self.title;
    }
}



@end

