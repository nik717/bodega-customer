//
//  ViewController.m
//  BodegaCustomer
//
//  Created by Siam Biswas on 1/31/16.
//  Copyright © 2016 iSoul. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)contact:(id)sender {
    
    NSString *recipients = @"mailto:info@mybodega.online?subject=Making contact with My Bodega Online LLC";
    NSString *body = @"&body= ";
    
    NSString *email = [NSString stringWithFormat:@"%@%@", recipients, body];
    email = [email stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:email]];
    
}

- (IBAction)web:(id)sender {
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"http://www.mybodega.online/"]];
}

@end
