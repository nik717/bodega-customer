//
//  CartTableViewCell.h
//  BodegaCustomer
//
//  Created by Siam Biswas on 2/16/17.
//  Copyright © 2017 iSoul. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Cart.h"

@protocol CartDelegate <NSObject>
-(void) cartUpdate;
@end

@interface CartTableViewCell : UITableViewCell

@property (nonatomic, assign) id<CartDelegate>  delegate;

@property (strong, nonatomic) Product *product;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *quantityLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalPriceLabel;

@end
