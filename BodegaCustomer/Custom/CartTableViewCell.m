//
//  CartTableViewCell.m
//  BodegaCustomer
//
//  Created by Siam Biswas on 2/16/17.
//  Copyright © 2017 iSoul. All rights reserved.
//

#import "CartTableViewCell.h"

@implementation CartTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)actionRemove:(id)sender {
    [Cart clearProduct:self.product];
    [self.delegate cartUpdate];
}


- (IBAction)actionPlus:(id)sender {
    [Cart addProduct:self.product];
    [self.delegate cartUpdate];
}


- (IBAction)actionMinus:(id)sender {
    [Cart removeProduct:self.product];
    [self.delegate cartUpdate];
}


@end
