//
//  CustomLabel.m
//  BodegaCustomer
//
//  Created by Apple on 2/23/16.
//  Copyright © 2016 iSoul. All rights reserved.
//

#import "CustomLabel.h"

@implementation CustomLabel



- (void)drawRect:(CGRect)rect {
    
    UIEdgeInsets insets = {0,20, 0, 0};
    [super drawTextInRect:UIEdgeInsetsInsetRect(rect, insets)];
}


@end
