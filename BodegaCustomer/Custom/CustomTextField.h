//
//  CustomTextField.h
//  BodegaCustomer
//
//  Created by Apple on 2/25/16.
//  Copyright © 2016 iSoul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomTextField : UITextField

@property (nonatomic, assign) UIEdgeInsets edgeInsets;

@end
