//
//  CustomTextField.m
//  BodegaCustomer
//
//  Created by Apple on 2/25/16.
//  Copyright © 2016 iSoul. All rights reserved.
//

#import "CustomTextField.h"

@implementation CustomTextField


- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.edgeInsets = UIEdgeInsetsZero;
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if(self){
        UIEdgeInsets insets = {0,10, 0,0};
        self.edgeInsets = insets;
    }
    return self;
}

- (CGRect)textRectForBounds:(CGRect)bounds {
    return [super textRectForBounds:UIEdgeInsetsInsetRect(bounds, self.edgeInsets)];
}

- (CGRect)editingRectForBounds:(CGRect)bounds {
    return [super editingRectForBounds:UIEdgeInsetsInsetRect(bounds, self.edgeInsets)];
}


@end
