//
//  ItemCell.h
//  BodegaCustomer
//
//  Created by Siam Biswas on 2/8/17.
//  Copyright © 2017 iSoul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ItemCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIView *view;
@property (weak, nonatomic) IBOutlet UILabel *subtitle;
@property (weak, nonatomic) IBOutlet UILabel *title;


@end
