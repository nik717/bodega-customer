//
//  SearchTableViewCell.h
//  BodegaCustomer
//
//  Created by Siam Biswas on 2/17/17.
//  Copyright © 2017 iSoul. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Cart.h"
#import "AppDelegate.h"


@interface SearchTableViewCell : UITableViewCell

@property (strong, nonatomic) Product *product;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;


@end
