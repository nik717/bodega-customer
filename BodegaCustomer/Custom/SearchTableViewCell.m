//
//  SearchTableViewCell.m
//  BodegaCustomer
//
//  Created by Siam Biswas on 2/17/17.
//  Copyright © 2017 iSoul. All rights reserved.
//

#import "SearchTableViewCell.h"

@implementation SearchTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)actionAdd:(id)sender {
    
    [Cart addProduct:self.product];
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] updateCartTabBadge];
}


@end
