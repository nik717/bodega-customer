//
//  TimeLabel.m
//  BodegaCustomer
//
//  Created by Apple on 2/25/16.
//  Copyright © 2016 iSoul. All rights reserved.
//

#import "TimeLabel.h"

@implementation TimeLabel

- (void)drawRect:(CGRect)rect {
    
    UIEdgeInsets insets = {0,0, 0, 25};
    [super drawTextInRect:UIEdgeInsetsInsetRect(rect, insets)];
}

@end
