//
//  TimeManagement.h
//  ObjectiveC
//
//  Created by user on 3/15/16.
//  Copyright © 2016 Riduan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TimeManagement : NSObject
{
    NSInteger nowTime;
    int weekday;
}


+ (TimeManagement*)getSharedInstance;
- (BOOL)isTimeInMiddle:(NSString *)startingTime lastTime:(NSString *)endingTime ;


@end
