//
//  TimeManagement.m
//  ObjectiveC
//
//  Created by user on 3/15/16.
//  Copyright © 2016 Riduan. All rights reserved.
//

#import "TimeManagement.h"

static TimeManagement *sharedInstance = nil;

@implementation TimeManagement

+(TimeManagement*)getSharedInstance{
    
    if (!sharedInstance) {
        
        sharedInstance = [[super allocWithZone:NULL]init];
    }
    
    return sharedInstance;
}




- (BOOL)isTimeInMiddle:(NSString *)startingTime lastTime:(NSString *)endingTime {
    
    NSString *firstTime = [TimeManagement convert12Hoursto24hours:startingTime ];
    
    NSString *lastTime = [TimeManagement convert12Hoursto24hours:endingTime];
    
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond fromDate:[NSDate date]];
    NSInteger currHr = [components hour];
    NSInteger currtMin = [components minute];
    nowTime =  (currHr*60) + currtMin;

    
    
    int stHr = [[[firstTime componentsSeparatedByString:@":"] objectAtIndex:0] intValue];
    int stMin = [[[firstTime componentsSeparatedByString:@":"] objectAtIndex:1] intValue];
    int enHr = [[[lastTime componentsSeparatedByString:@":"] objectAtIndex:0] intValue];
    int enMin = [[[lastTime componentsSeparatedByString:@":"] objectAtIndex:1] intValue];
    
    int formStTime = (stHr*60)+stMin;
    int formEnTime = (enHr*60)+enMin;
    
    
    
    if(nowTime >= formStTime && nowTime <= formEnTime)
    {
        return YES;
        
    } else {
        return NO;
    }
    
}



+ (NSString *)convert12Hoursto24hours:(NSString *) time {
    
   
    NSDate *date = [[TimeManagement dateReader] dateFromString: time];
    NSString *convertedString = [[TimeManagement dateReader2] stringFromDate:date];
    
    
    return convertedString;
    
}

+ (BOOL)is24Hour{
    
    NSString *dateString = [[TimeManagement dateReader3] stringFromDate:[NSDate date]];
    NSRange amRange = [dateString rangeOfString:[[TimeManagement dateReader3] AMSymbol]];
    NSRange pmRange = [dateString rangeOfString:[[TimeManagement dateReader3] PMSymbol]];
    
    
    return  (amRange.location == NSNotFound && pmRange.location == NSNotFound);
    
}


+ (NSDateFormatter *)dateReader
{
    NSMutableDictionary *dictionary = [[NSThread currentThread] threadDictionary];
    NSDateFormatter *dateReader = [dictionary objectForKey:@"SCDateReader"];
    if (!dateReader)
    {
        dateReader = [[NSDateFormatter alloc] init];
        dateReader.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        dateReader.dateFormat = ([self is24Hour] ? @"HH:mm" : @"hh:mm a");
        [dictionary setObject:dateReader forKey:@"SCDateReader"];
    }
    return dateReader;
}

+ (NSDateFormatter *)dateReader2
{
    NSMutableDictionary *dictionary = [[NSThread currentThread] threadDictionary];
    NSDateFormatter *dateReader = [dictionary objectForKey:@"SCDateReader2"];
    if (!dateReader)
    {
        dateReader = [[NSDateFormatter alloc] init];
        dateReader.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        dateReader.dateFormat = @"HH:mm";
        [dictionary setObject:dateReader forKey:@"SCDateReader2"];
    }
    return dateReader;
}

+ (NSDateFormatter *)dateReader3
{
    NSMutableDictionary *dictionary = [[NSThread currentThread] threadDictionary];
    NSDateFormatter *dateReader = [dictionary objectForKey:@"SCDateReader3"];
    if (!dateReader)
    {
        dateReader = [[NSDateFormatter alloc] init];
        dateReader.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        [dateReader setDateStyle:NSDateFormatterNoStyle];
        [dateReader setTimeStyle:NSDateFormatterShortStyle];
        [dictionary setObject:dateReader forKey:@"SCDateReader3"];
    }
    return dateReader;
}



@end
