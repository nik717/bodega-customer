//
// Created by Jose Gustavo Rodriguez Baldera on 5/24/14.
// Copyright (c) 2014 Jose Gustavo Rodriguez Baldera. All rights reserved.
//

#import "JSONModel.h"
#import "Product.h"
#import "Cart.h"
@interface CartItem : JSONModel


@property (strong, nonatomic) Product *product;
//@property (strong, nonatomic) Cart *cart;
@property (assign, nonatomic) int quantity;

- (id)initWithProduct:(Product *)product;
- (id)initWithProduct:(Product *)product andQuantity:(int)quantity;
@end
