//
//  Category.h
//  BodegaStore
//
//  Created by Siam Biswas on 12/18/16.
//  Copyright © 2016 iSoul. All rights reserved.
//

#import "JSONModel.h"

@protocol Category

@end

@interface Category : JSONModel

@property (strong, nonatomic) NSString *id;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *storeid;
@property (strong, nonatomic) NSString *status;
@property (strong, nonatomic) NSString *created_at;
@property (strong, nonatomic) NSString *updated_at;

+(BOOL)propertyIsOptional:(NSString*)propertyName;
+(BOOL)propertyIsIgnored:(NSString*)propertyName;

@end

