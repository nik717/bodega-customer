//
//  Conversation.h
//  BodegaCustomer
//
//  Created by Apple on 3/14/16.
//  Copyright © 2016 iSoul. All rights reserved.
//

#import "JSONModel.h"
#import "ConversationUser.h"

@interface Conversation : JSONModel

@property (assign, nonatomic) int error;
@property (strong, nonatomic) NSArray <ConversationUser> *records;

+(BOOL)propertyIsOptional:(NSString*)propertyName;
+(BOOL)propertyIsIgnored:(NSString*)propertyName;

@end
