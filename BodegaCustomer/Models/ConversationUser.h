//
//  ConversationUser.h
//  BodegaCustomer
//
//  Created by Apple on 3/14/16.
//  Copyright © 2016 iSoul. All rights reserved.
//

#import "JSONModel.h"

@protocol ConversationUser

@end

@interface ConversationUser : JSONModel

@property (assign, nonatomic) int id;
@property (strong, nonatomic) NSString  *sender_name;
@property (strong, nonatomic) NSString *receiver_name;
@property (strong, nonatomic) NSString *message;
@property (strong, nonatomic) NSString *date;
@property (assign, nonatomic) int sent;
@property (assign, nonatomic) int sender_id;
@property (assign, nonatomic) int receiver_id;

+(BOOL)propertyIsOptional:(NSString*)propertyName;
+(BOOL)propertyIsIgnored:(NSString*)propertyName;

@end
