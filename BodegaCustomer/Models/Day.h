//
//  Day.h
//  BodegaCustomer
//
//  Created by Siam Biswas on 2/3/16.
//  Copyright © 2016 iSoul. All rights reserved.
//

#import "JSONModel.h"
#import "Time.h"

@interface Day : JSONModel

@property (strong, nonatomic) Time  *startTime;
@property (strong, nonatomic) Time  *endTime;

@end
