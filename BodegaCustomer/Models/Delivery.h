//
//  Delivery.h
//  BodegaCustomer
//
//  Created by Siam Biswas on 2/3/16.
//  Copyright © 2016 iSoul. All rights reserved.
//

#import "JSONModel.h"

@interface Delivery : JSONModel

@property (assign, nonatomic) int  deliveryOption;
@property (strong, nonatomic) NSString  *value;

@end
