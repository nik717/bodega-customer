//
//  DeliveryFees.h
//  BodegaCustomer
//
//  Created by Siam Biswas on 2/3/16.
//  Copyright © 2016 iSoul. All rights reserved.
//

#import "JSONModel.h"
#import "Delivery.h"

@interface DeliveryFees : JSONModel

@property (strong, nonatomic) Delivery  *dZero;
@property (strong, nonatomic) Delivery  *dOne;
@property (strong, nonatomic) Delivery  *dTwo;
@property (strong, nonatomic) Delivery  *dThree;
@property (strong, nonatomic) Delivery  *dFour;

+(BOOL)propertyIsOptional:(NSString*)propertyName;
+(BOOL)propertyIsIgnored:(NSString*)propertyName;



@end
