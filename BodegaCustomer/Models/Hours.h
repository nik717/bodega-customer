//
//  Hours.h
//  BodegaCustomer
//
//  Created by Siam Biswas on 2/3/16.
//  Copyright © 2016 iSoul. All rights reserved.
//

#import "JSONModel.h"
#import "Day.h"

@interface Hours : JSONModel

@property (strong, nonatomic) Day  *MonDay;
@property (strong, nonatomic) Day  *TuesDay;
@property (strong, nonatomic) Day  *WednesDay;
@property (strong, nonatomic) Day  *ThursDay;
@property (strong, nonatomic) Day  *FriDay;
@property (strong, nonatomic) Day  *SaturDay;
@property (strong, nonatomic) Day  *SunDay;



@end
