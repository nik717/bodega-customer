//
//  Images.h
//  BodegaCustomer
//
//  Created by Siam Biswas on 2/3/16.
//  Copyright © 2016 iSoul. All rights reserved.
//

#import "JSONModel.h"

@interface Images : JSONModel

@property (strong, nonatomic) NSString  *profilePic;
@property (strong, nonatomic) NSString  *officePic;
@property (strong, nonatomic) NSString  *legalPic;

+(BOOL)propertyIsOptional:(NSString*)propertyName;
+(BOOL)propertyIsIgnored:(NSString*)propertyName;

@end
