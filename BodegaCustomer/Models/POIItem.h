//
//  POIItem.h
//  BodegaCustomer
//
//  Created by Siam Biswas on 2/8/17.
//  Copyright © 2017 iSoul. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <Google-Maps-iOS-Utils/GMUMarkerClustering.h>
#import <GoogleMaps/GoogleMaps.h>

// Point of Interest Item which implements the GMUClusterItem protocol.
@interface POIItem : NSObject<GMUClusterItem>

@property(nonatomic) CLLocationCoordinate2D position;
@property(nonatomic, strong) NSString *name;
@property(nonatomic, strong) NSString *snippet;
@property(nonatomic, strong) UIImage *icon;
@property(nonatomic, strong) NSString *identity;



- (instancetype)initWithPosition:(CLLocationCoordinate2D)position name:(NSString *)name snippet:(NSString *)snippet icon:(UIImage *)icon identity:(NSString *)identity;

@end
