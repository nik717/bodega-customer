//
//  POIItem.m
//  BodegaCustomer
//
//  Created by Siam Biswas on 2/8/17.
//  Copyright © 2017 iSoul. All rights reserved.
//

#import "POIItem.h"

@implementation POIItem

- (instancetype)initWithPosition:(CLLocationCoordinate2D)position name:(NSString *)name snippet:(NSString *)snippet icon:(UIImage *)icon identity:(NSString *)identity{
    
    self.position = position;
    self.name = name;
    self.snippet = snippet;
    self.icon = icon;
    self.identity = identity;
    

    
    return self;
}

@end
