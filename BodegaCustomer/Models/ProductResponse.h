//
//  ProductResponse.h
//  BodegaStore
//
//  Created by Siam Biswas on 12/18/16.
//  Copyright © 2016 iSoul. All rights reserved.
//

#import "JSONModel.h"
#import "Product.h"

@interface ProductResponse : JSONModel

@property (assign, nonatomic) int error;
@property (strong, nonatomic) NSString  *message;
@property (strong, nonatomic) NSArray <Product>  *records;

@end
