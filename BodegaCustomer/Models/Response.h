//
//  Response.h
//  BodegaCustomer
//
//  Created by Siam Biswas on 2/2/16.
//  Copyright © 2016 iSoul. All rights reserved.
//

#import "JSONModel.h"

@interface Response : JSONModel

@property (assign, nonatomic) int success;
@property (strong, nonatomic) NSString  *message;
@property (assign, nonatomic) int error;

+(BOOL)propertyIsOptional:(NSString*)propertyName;
+(BOOL)propertyIsIgnored:(NSString*)propertyName;


@end
