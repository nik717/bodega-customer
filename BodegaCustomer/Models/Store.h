//
//  Store.h
//  BodegaCustomer
//
//  Created by Siam Biswas on 2/2/16.
//  Copyright © 2016 iSoul. All rights reserved.
//

#import "JSONModel.h"

@protocol Store
@end

@interface Store : JSONModel

@property (assign, nonatomic) int storeId;
@property (strong, nonatomic) NSString  *storeName;
@property (assign, nonatomic) double storeLatitude;
@property (assign, nonatomic) double storeLongitude;
@property (strong, nonatomic) NSString *storeLocation;
@property (strong, nonatomic) NSString *storeStreet;

+(BOOL)propertyIsOptional:(NSString*)propertyName;
+(BOOL)propertyIsIgnored:(NSString*)propertyName;

@end
