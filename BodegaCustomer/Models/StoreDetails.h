//
//  StoreDetails.h
//  BodegaCustomer
//
//  Created by Siam Biswas on 2/3/16.
//  Copyright © 2016 iSoul. All rights reserved.
//

#import "Store.h"
#import "Hours.h"
#import "DeliveryFees.h"

@interface StoreDetails : Store


@property (strong, nonatomic) NSString  *storeContact;
@property (strong, nonatomic) NSString *storeLandLine;
@property (strong, nonatomic) NSString *storeWebAddress;
@property (strong, nonatomic) NSString  *storeLegalName;
@property (strong, nonatomic) NSString *storeOwnerFirstName;
@property (strong, nonatomic) NSString *storeOwnerLastName;
@property (strong, nonatomic) NSString  *storeOwnerEmail;
@property (strong, nonatomic) NSString *storeTaxAmount;
@property (strong, nonatomic) NSString *storeActiveContact;
@property (strong, nonatomic) Hours *storeHours;
@property (strong, nonatomic) DeliveryFees  *storeDeliveryFess;


@end
