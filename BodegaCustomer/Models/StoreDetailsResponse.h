//
//  StoreDetailsResponse.h
//  BodegaCustomer
//
//  Created by Siam Biswas on 2/3/16.
//  Copyright © 2016 iSoul. All rights reserved.
//

#import "JSONModel.h"
#import "StoreDetailsResult.h"

@interface StoreDetailsResponse : JSONModel

@property (assign, nonatomic) int success;
@property (strong, nonatomic) NSString  *message;
@property (strong, nonatomic) StoreDetailsResult  *results;


@end
