//
//  StoreDetailsResult.h
//  BodegaCustomer
//
//  Created by Siam Biswas on 2/3/16.
//  Copyright © 2016 iSoul. All rights reserved.
//

#import "JSONModel.h"
#import "StoreDetails.h"
#import "Product.h"
#import "Images.h"

@interface StoreDetailsResult : JSONModel

@property (strong, nonatomic) StoreDetails  *storeDetails;
@property (strong, nonatomic) NSArray <Product>  *itemlist;
@property (strong, nonatomic) Images *storeImages;

+(BOOL)propertyIsOptional:(NSString*)propertyName;
+(BOOL)propertyIsIgnored:(NSString*)propertyName;




@end
