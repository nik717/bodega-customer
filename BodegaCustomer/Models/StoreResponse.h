//
//  StoreResponse.h
//  BodegaCustomer
//
//  Created by Siam Biswas on 2/2/16.
//  Copyright © 2016 iSoul. All rights reserved.
//

#import "JSONModel.h"
#import "StoreResult.h"

@interface StoreResponse : JSONModel

@property (assign, nonatomic) int success;
@property (strong, nonatomic) NSString  *message;
@property (strong, nonatomic) StoreResult  *results;

@end
