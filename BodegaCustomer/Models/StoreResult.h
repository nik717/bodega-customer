//
//  StoreResult.h
//  BodegaCustomer
//
//  Created by Siam Biswas on 2/2/16.
//  Copyright © 2016 iSoul. All rights reserved.
//

#import "JSONModel.h"
#import "Store.h"

@interface StoreResult : JSONModel

@property (strong, nonatomic) NSArray <Store>  *shopList;

@end
