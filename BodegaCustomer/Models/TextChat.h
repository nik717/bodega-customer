//
//  TextChat.h
//  BodegaCustomer
//
//  Created by Workspace Infotech on 2/5/16.
//  Copyright © 2016 iSoul. All rights reserved.
//

#import "JSONModel.h"

@interface TextChat : JSONModel

@property (assign,nonatomic) int id;
@property (assign,nonatomic) int user_id;
@property (assign,nonatomic) int conversation_id;
@property (strong,nonatomic) NSString *body;
@property (strong,nonatomic) NSString *created_at;


@end
