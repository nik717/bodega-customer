//
//  Time.h
//  BodegaCustomer
//
//  Created by Siam Biswas on 2/3/16.
//  Copyright © 2016 iSoul. All rights reserved.
//

#import "JSONModel.h"

@interface Time : JSONModel

@property (strong, nonatomic) NSString  *time;
@property (strong, nonatomic) NSString *ind;

@end
