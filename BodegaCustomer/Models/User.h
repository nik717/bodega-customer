//
//  User.h
//  BodegaCustomer
//
//  Created by Siam Biswas on 2/2/16.
//  Copyright © 2016 iSoul. All rights reserved.
//

#import "JSONModel.h"

@interface User : JSONModel

@property (assign, nonatomic) int userId;
@property (strong, nonatomic) NSString  *userFirstName;
@property (strong, nonatomic) NSString *userLastName;
@property (strong, nonatomic) NSString *userEmail;
@property (assign, nonatomic) int userType;
@property (assign, nonatomic) int userDeviceId;
@property (assign, nonatomic) int userrlMethod;
@property (strong, nonatomic) NSString *userContact;

+(BOOL)propertyIsOptional:(NSString*)propertyName;
+(BOOL)propertyIsIgnored:(NSString*)propertyName;


@end
