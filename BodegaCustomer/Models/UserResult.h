//
//  UserResult.h
//  BodegaCustomer
//
//  Created by Siam Biswas on 2/2/16.
//  Copyright © 2016 iSoul. All rights reserved.
//

#import "JSONModel.h"
#import "User.h"

@interface UserResult : JSONModel

@property (strong, nonatomic) User  *userInformation;

@end
