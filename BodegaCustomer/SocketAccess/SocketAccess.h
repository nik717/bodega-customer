//
//  SocketAccess.h
//  BodegaCustomer
//
//  Created by Siam Biswas on 3/1/16.
//  Copyright © 2016 iSoul. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "SocketIO.h"


@protocol SocketAccessDelegate <NSObject>

@required
-(void)receivedResponseWith:(NSDictionary*)data tag:(NSString*) tag;
@end

@interface SocketAccess : NSObject <SocketIODelegate>

@property (nonatomic,assign)  id<SocketAccessDelegate> delegate;
@property (nonatomic,strong)  SocketIO* socket;
@property (strong, nonatomic) UITabBarItem *item;
@property (assign, nonatomic) int badgeValue;


+(SocketAccess*)getSharedInstance;
-(void)connect;
-(void)disconnect;
-(void) registerOffline;
-(void) registerOnline;
-(void)sendMsg:(NSDictionary*) param;
-(void) emptyBadge;


@end
