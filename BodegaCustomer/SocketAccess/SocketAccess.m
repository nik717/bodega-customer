//
//  SocketAccess.m
//  BodegaCustomer
//
//  Created by Siam Biswas on 3/1/16.
//  Copyright © 2016 iSoul. All rights reserved.
//

#import "SocketAccess.h"
#import "ApiAccess.h"



static SocketAccess *sharedInstance = nil;

@class SocketIOClient;

@interface SocketAccess ()



@end

@implementation SocketAccess

+(SocketAccess*)getSharedInstance{
    
    if (!sharedInstance) {
        
        sharedInstance = [[super allocWithZone:NULL]init];
        [sharedInstance connect];
    }
    
    return sharedInstance;
}

-(void) connect
{
    
    
    self.socket = [[SocketIO alloc] initWithDelegate:self];
    
    // you can update the resource name of the handshake URL
    // see https://github.com/pkyeck/socket.IO-objc/pull/80
    // [socketIO setResourceName:@"whatever"];
    
    // if you want to use https instead of http
    // socketIO.useSecure = YES;
    
    // pass cookie(s) to handshake endpoint (e.g. for auth)
    NSDictionary *properties = [NSDictionary dictionaryWithObjectsAndKeys:
                                @"192.169.227.95", NSHTTPCookieDomain,
                                @"/", NSHTTPCookiePath,
                                @"auth", NSHTTPCookieName,
                                @"56cdea636acdf132", NSHTTPCookieValue,
                                nil];
    NSHTTPCookie *cookie = [NSHTTPCookie cookieWithProperties:properties];
    NSArray *cookies = [NSArray arrayWithObjects:cookie, nil];
    
    self.socket.cookies = cookies;
    
    // connect to the socket.io server that is running locally at port 3000
    [self.socket connectToHost:@"192.169.227.95" onPort:3000];
    
}

-(void) registerOnline
{
    
    NSDictionary *param = @{@"userid" :[NSString stringWithFormat:@"%d",[ApiAccess getSharedInstance].getUser.userId]};
    [self.socket sendEvent:@"register_online" withData:@[param]];
    

    
    SocketIOCallback cb = ^(id argsData) {
        NSDictionary *response = argsData;
        NSLog(@"exist message received");
        self.badgeValue = self.badgeValue + 1;
        self.item.badgeValue = [NSString stringWithFormat:@"%d",self.badgeValue];
        
        if (self.delegate) {
            [self.delegate receivedResponseWith:response tag:@"chat.messages"];
        }
    };
    [self.socket sendMessage:@"server_message" withAcknowledge:cb];
}

-(void) emptyBadge{
    
    self.badgeValue = 0;
    self.item.badgeValue = nil;

}

-(void) registerOffline
{
    NSDictionary *param = @{@"userid" :[NSString stringWithFormat:@"%d",[ApiAccess getSharedInstance].getUser.userId]};
    [self.socket sendEvent:@"register_offline" withData:@[param]];
}



-(void) disconnect
{
    [self.socket disconnect];
    
}

-(void)sendMsg:(NSDictionary*) param
{
    [self.socket sendEvent:@"client_message" withData:@[param]];
}


# pragma mark -
# pragma mark socket.IO-objc delegate methods

- (void) socketIODidConnect:(SocketIO *)socket
{
    NSLog(@"socket.io connected.");
}

- (void) socketIO:(SocketIO *)socket didReceiveEvent:(SocketIOPacket *)packet
{
    
}

- (void) socketIO:(SocketIO *)socket onError:(NSError *)error
{
    if ([error code] == SocketIOUnauthorized) {
        NSLog(@"not authorized");
    } else {
        NSLog(@"onError() %@", error);
    }
}


- (void) socketIODidDisconnect:(SocketIO *)socket disconnectedWithError:(NSError *)error
{
    NSLog(@"socket.io disconnected. did error occur? %@", error);
}


@end
 
